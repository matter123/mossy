;simple file that performs an exception that is trapped by the kernel
[SECTION .text]
[GLOBAL start]
start:
	push dword 0xDEAD1234
	int 0x30
	jmp $;infinate loop incase sys call fails
