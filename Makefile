loopback=$(shell losetup -f)

.PHONY: all
all: build install run

.PHONY: build
build: mk_kernel mk_exec_test

.PHONY: clean
clean: cl_kernel cl_exec_test

.PHONY: install
install: setup_rd update

.PHONY: clean

.PHONY: run
run:
	@echo "running emulator (qemu)"
	@bash scripts/run_emulator.sh `pwd` 0 &

.PHONY: update
update:
	@echo updating iso
	@cp -f kernel/kernel iso/kernel
	@cp -f initrd.rd iso/initrd.rd
	@grub2-mkrescue -o bootable.iso iso
.PHONY: setup_rd
setup_rd:
	@cp exec_test/exec_test rdfiles/exec_test.tst
	@tools/packrd.py

.PHONY: mk_kernel
mk_kernel:
	@echo compiling kernel
	@cd kernel && make --no-print-directory build

.PHONY: mk_exec_test
mk_exec_test:
	@echo compiling execute test
	@cd exec_test && make --no-print-directory build
	
.PHONY: cl_kernel
cl_kernel:
	@echo cleaning kernel
	@cd kernel && make --no-print-directory clean

.PHONY: cl_exec_test
cl_exec_test:
	@echo cleaning execute test
	@cd exec_test && make --no-print-directory clean
