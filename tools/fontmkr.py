#! /usr/bin/python
# Copyright 2013 Matthew Fosdick

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

         # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import struct
import os
import os.path as path
import Image

def help():
	print "fontmkr, version 1.0 copyright Matthew Fosdick 2013"
	print "fontmkr [<output> <input>]"
	print 'creates a binary "font file" from the given input image saved to the given output'
	print "if no arguments are passed, 'def.fnt' is the assumed output"
	print "and 'font.png' is the assumed input"

def writeByte(output,num):
	output.write(struct.pack("B",num))
	
#I like C, going to ignore PEP 8
argv=sys.argv
argc=len(argv)

input=path.abspath("font.png")
outputp=path.abspath("def.fnt")

if argc == 2 and argv[1] == "h":
	help()
	exit(0)

if argc == 3:
	outputp=path.abspath(argv[1])
	input=path.abspath(argv[2])

if not path.exists(input) or not path.isfile(input):
	print "non existent input, or not a file: "+input
	exit(1)

fnt=Image.open(input);
output=open(outputp,"wb");



