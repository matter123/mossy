#!/bin/bash
loopback=`losetup -f`
sudo losetup $loopback floppy.img
sudo mount $loopback /mnt/
sudo nautilus /mnt
sudo umount $loopback
sudo losetup -d $loopback
