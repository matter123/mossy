#!/bin/bash
cd ..
qemu-system-i386 -vga cirrus -name MOSSY -cpu pentium -m 8  -serial vc -soundhw pcspk -cdrom $1/bootable.iso &
sleep 1
wmctrl -a "QEMU (MOSSY)"
wmctrl -r "QEMU (MOSSY)" -b toggle,maximized_vert,maximized_horz

