/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
//implementation of malloc and friends
#include "common.h"
#include "mem.h"
#include <string.h>

void * k_alloc(uint32_t size);

struct malloc_head * start_head;

struct malloc_head {
	uint16_t magic;
	int free   :2;
	int sys    :2;
	int resv   :12;
	uint32_t size;
	struct malloc_head *next;
	struct malloc_head *prev;

};
#define HEAD_SIZE (sizeof(struct malloc_head))

static bool malloc_enabled=false;

static uint32_t init_stucture_count;
static uint8_t *startmem;
extern uint8_t *nresv_mem;

void init_malloc() {
	int max=(k_alloc_addr()/0x400)+(k_alloc_addr()%0x400?1:0)+100;//100k more of reserved ram
	//reserve all important ram
	for(int i=0;i<max;i++) {
		nresv_mem[i/8]&=~(1<<(i%8));
	}
	init_stucture_count=((ramsize()/0x400)-max);

	//set up malloc structures in the rest
	startmem=(uint8_t *)(max*0x400);
	start_head=(struct malloc_head *)(startmem);
	start_head->free=true;
	start_head->magic=0xDEAD;
	start_head->resv=0;
	start_head->sys=0;
	start_head->size=((ramsize()*0x400)-(uint32_t)startmem)-HEAD_SIZE;
	start_head->prev=NULL;
	start_head->next=NULL;
	malloc_enabled = true;
}

void split(struct malloc_head *bh,size_t size);
void merge(struct malloc_head *head);
void move_head(struct malloc_head *header,void *loc);
void dumb_merge(struct malloc_head *left, struct malloc_head *right);

void * malloc(size_t size) {
	if(!malloc_enabled)return k_alloc(size);
	struct malloc_head *use=NULL;
	struct malloc_head *head=start_head;
	while(head!=NULL&&head->magic==0xDEAD) {
		if(head->free==true&&head->sys==false&&head->size>=size) {
			if((head->size-HEAD_SIZE)>size)split(head,size);
			use=head;
			break;
		}
		head=head->next;
	}
	if(use==NULL)return NULL;
	use->free=false;
	return ((void *)(use))+HEAD_SIZE;
}

void * calloc(size_t num, size_t size) {
	void * ret;
	if(!malloc_enabled)ret = k_alloc(num*size);
	else ret=malloc(num*size);
	memset(ret,0,num*size);
	return ret;
}

void free(void *ptr) {
	if(!malloc_enabled)return;
	struct malloc_head *head=(struct malloc_head *)(ptr-HEAD_SIZE);
	if(head->magic!=0xDEAD)return;
	head->free=true;
	merge(head);
}

void * realloc(void *ptr,size_t size) {
	if(!malloc_enabled)return k_alloc(size);
	if(size==0)free(ptr);
	if(ptr==NULL)return malloc(size);
	struct malloc_head *cur_head=(struct malloc_head *)(ptr-HEAD_SIZE);
	size_t old_size=cur_head->size;
	size_t cur_size=cur_head->size;
	if(size>cur_size) {
		while(cur_size<size && cur_head->next!=NULL &&
				cur_head->magic==0xDEAD && cur_head->next->free==true) {
			dumb_merge(cur_head,cur_head->next);
			cur_size=cur_head->size;
		}
		if(cur_size<size) {
			//merging failed, memmove to a new spot
			void * newptr=malloc(size);
			if(newptr==NULL)return NULL;
			memmove(newptr,ptr,old_size);
			free(ptr);
		}
	}else {
		if(cur_head->next->free==true) {
			move_head(cur_head->next,(void *)((size_t)cur_head)+HEAD_SIZE+size);
		}
	}
	return NULL;
}

void malloc_test() {
	void * first=malloc(4);
	void * second=malloc(4);
	free(first);
	free(second);
	void * third=malloc(8);
	debug("head size=%x first=%p second=%p third=%p ",HEAD_SIZE,first,second,third);
	end();
}

void split(struct malloc_head *bh,size_t size) {
	struct malloc_head *head=(struct malloc_head *)(((void *)(bh))+HEAD_SIZE+size);
	head->next=bh->next;
	bh->next=head;
	head->prev=bh;
	head->free=1;
	head->resv=0;
	head->magic=0xDEAD;
	head->sys=0;
	head->size=bh->size-HEAD_SIZE-size;
	bh->size=size;
	head->next->prev=head;
}

void dumb_merge(struct malloc_head *left, struct malloc_head *right) {
	left->next=right->next;
	left->next->prev=left;
	left->size=left->size+right->size+HEAD_SIZE;
	right->magic=0;
}

void merge(struct malloc_head *head) {
	struct malloc_head *left=head;
	while(left->next!=NULL&&left->next->free==true) {
		uint32_t tsize=left->size+left->next->size+HEAD_SIZE;
		if(tsize>0x400)break;
		dumb_merge(left,left->next);
	}
	while(left->prev!=NULL&&left->prev->free==true) {
		uint32_t tsize=left->size+left->prev->size+HEAD_SIZE;
		if(tsize>0x400)return;
		dumb_merge(left->prev,left);
		left=left->prev;
	}
}

void move_head(struct malloc_head *header,void *loc) {
	uint32_t adj=((uint32_t)header-(uint32_t)loc);
	memmove(loc,header,HEAD_SIZE);
	header=(struct malloc_head *)loc;
	header->next->prev=header;
	header->prev->next=header;
	header->prev->size-=adj;
	header->next->size+=adj;
}