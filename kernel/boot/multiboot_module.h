/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __MULTIBOOT_MODULE_H
#define __MULTIBOOT_MODULE_H
#include "../common.h"
#include "../multiboot.h"
#include <string.h>

void init_modules(multiboot_t *multi);

uint32_t getEndAddr();

uint32_t getModuleCount();

uint32_t getSignature(uint32_t module);

typedef struct mod_struct {
	void * mod_start;
	uint32_t mod_end;
	string str;
	uint32_t resv;
}mod_struct_t;

mod_struct_t *getModule(uint32_t module);

#endif

