/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "initrd.h"
#include "multiboot_module.h"
#include "../vfs/vfs.h"
#include "../vfs/node.h"
#include "../panic.h"
#include "../debug.h"
#include "../mem.h"
#include <stream.h>
#include <kmath.h>

mod_struct_t * rd_base=0;
STREAM rd;
node_t *initrd;
dev_t * initrddev;
uint32_t filecount=0;

typedef struct file_head {
	char name[16];
	uint32_t size;
}file_head_t;

typedef struct {
	file_head_t *head;
	uint32_t offset;
}file_t;

file_head_t *fileHead;
file_t *files;


//no write, why would you write?
int readRDFile(void *node,uint32_t size,uint32_t offset,void *buffer);

void init_initrd() {
	debug("unpacking ramdisk\n");
	for(uint32_t i=0;i<getModuleCount();i++) {
		if(getSignature(i)==0xAA55AA55) { //mossy ram disk signature
			rd_base=getModule(i);
			break;
		}
	}
	if(!rd_base)panic(PANIC_NO_RAMDISK);
	//5 system calls, need to make nice
	initrd=createNode(getSystemName("INITRD.DIR"),findChild(getRoot(),getSystemName("DEVICE.DIR")),
			VFS_NODE_SYSTEM|VFS_NODE_DIR);
	rd=createStream(rd_base->mod_start,rd_base->mod_end-(uint32_t)rd_base->mod_start);
	initrddev=(dev_t *)malloc(sizeof(dev_t));
	initrddev->read_func=&readRDFile;

	//double check
	if(!readU32(rd)==0xAA55AA55)panic(PANIC_NO_RAMDISK);

	//version check
	if(!readU32(rd)==0x1)panic(PANIC_NO_RAMDISK);

	filecount=readU32(rd);
	fileHead=(file_head_t *)readX(rd,sizeof(file_head_t)*filecount);
	files=(file_t *)malloc(sizeof(file_t)*filecount);

	uint32_t offset=seekpos(rd);
	for(unsigned int i=0;i<filecount;i++) {
		files[i].head=fileHead+i;
		files[i].offset=offset+4;
		offset+=files[i].head->size+4;
	}

	//double check
	uint32_t seekps=seekpos(rd);
	seek(rd,streamLength(rd)-4);
	if(!readU32(rd)==0xFFFFFFFF)panic(PANIC_NO_RAMDISK);
	seek(rd,seekps);

	//install nodes
	for(unsigned int i=0;i<filecount;i++) {
		string name=(string)malloc(17);
		memcpy((uint8_t *)name,(uint8_t *)files[i].head->name,16);
		name[16]=0;
		node_t *child=createNode(name,initrd,VFS_NODE_SYSTEM|VFS_NODE_FILE);
		child->size=files[i].head->size;
		child->device=(void *)initrddev;
	}

	if(initrd->child_count!=filecount)panic(PANIC_NO_RAMDISK);

}
bool hackstrcmp(char *rds,char *node) {
	while(*node && *rds++==*node++);
	if(*node==0)return true;
	return false;
}

int readRDFile(void *nodev,uint32_t size,uint32_t offset,void *buffer) {
	//find file
	node_t * node=(node_t *)nodev;
	debug("reading: %s",node->name);
	int file=0;
	for(unsigned int i=0;i<filecount;i++) {
		if(hackstrcmp(files[i].head->name,node->name)) {
			file=i;
			break;
		}
	}

	//read file
	int copy=min(files[file].head->size-offset,size);
	seek(rd,files[file].offset+offset);
	memcpy((uint8_t *)buffer,readX(rd,copy),copy);
	return copy;
}
