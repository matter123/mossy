/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "multiboot_module.h"
#include <kmath.h>

uint32_t mod_count;
uint32_t mod_struct_base;
uint32_t max_addr=0;

void init_modules(multiboot_t *multi) {
	debug("loading multiboot modules\n");
	mod_count=multi->mods_count;
	mod_struct_base=multi->mods_addr;
	uint32_t mod_struct_btemp=mod_struct_base;
	uint32_t c_mod=0;
	while(c_mod++<mod_count) {
		mod_struct_t *ms=(mod_struct_t *)mod_struct_btemp;
		max_addr=max(max_addr,ms->mod_end);
		mod_struct_btemp+=sizeof(mod_struct_t);
	}
	
}

uint32_t getEndAddr() {
	return max_addr;
}

uint32_t getModuleCount() {
	return mod_count;
}

uint32_t getSignature(uint32_t module) {
	mod_struct_t *ms=getModule(module);
	uint32_t *sig=(uint32_t *)ms->mod_start;
	return sig[0];
}

mod_struct_t *getModule(uint32_t module) {
	mod_struct_t *ms=(mod_struct_t *)(mod_struct_base+sizeof(mod_struct_t)*module);
	return ms;
}
