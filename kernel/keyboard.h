/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include "common.h"
#include "x86/irq.h"
#include "ps2.h"

int init_keyboard(int port);

typedef struct key {
    uint8_t extended : 1;
    uint8_t released : 1;
    uint8_t pausebut : 1;
    uint8_t printbut : 1;
    uint8_t reserved : 4;
    uint8_t letter;
} key_t;

void keyboard_install_keycode_interp(void (*key_interp)(uint16_t key));

bool isKeyPressed(uint8_t key);

#endif

