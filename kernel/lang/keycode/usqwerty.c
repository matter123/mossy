/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include "usqwerty.h"
#include "../../keyboard.h"
#include "../../debug.h"

#include <string.h>
#ifdef LANG_EN_US

void *callback_func[8];
uint8_t next=0;
void keycode_interp(uint16_t keycode);

void install_USQWERTY() {
	keyboard_install_keycode_interp(&keycode_interp);
}

void install_callback(void(*callback)(bool pressed, uint8_t letter)) {
	if (next>=8)return;
	callback_func[next++]=callback;
}

char ascii[255]={
	 0 , 'A', 'B', 'C',
	'D', 'E', 'F', 'G',
	'H', 'I', 'J', 'K',
	'L', 'M', 'N', 'O',
	'P', 'Q', 'R', 'S',
	'T', 'U', 'V', 'W',
	'X', 'Y', 'Z', '0',
	'1', '2', '3', '4',
	'5', '6', '7', '8',
	'9',  1 ,  2 ,  3 ,
	 4 ,  5 ,  6 ,  7 ,
	0xB, 0xC, 0xE, 0xF,
	0x10,0x8, 0x9, 0xA,
	0xD, 0x11, 0x12, 0x13,
	0x14,'`', '-', '=',
	'[', ']', '\\',';',
	'\'',',', '.', '/',
	0x15,
	0x16, 0x17, 0x18, 0x19,
	0x1A, 0x1C, 0x1D, 0x1E,
	0x1F, ' ', 0x1B, 0x7F,
	0x80, 0x81, 0x82, 0x83,
	0x84, 0x85, 0x86, 0x87,
	0x88, 0x89, 0x8A, 0x8B,
	0x8C, 0x8D, 0x8E, 0x8F,
	0x90, 0x91, 0x92, 0x93,
	0x94, 0x95, 0x96, 0x97,
	0x98, 0x99, 0x9A, 0x9B,
};

void keycode_interp(uint16_t keycode) {
	uint8_t asciic=ascii[keycode&0x7FFF];
	//if(asciic<0x20)
	//debug("%x",asciic);
	bool released=(keycode&~0x7FFF);
	for (int i=0; i<8; i++) {
		if (callback_func[i]) {
			void(*callback)(bool pressed, uint8_t letter)=callback_func[i];
			callback(!released, asciic);
		}
	}
}
#endif
