/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#ifndef __PS2_H
#define __PS2_H
#include "common.h"


typedef enum {
    UNKNOWN, MOUSE, KEYBOARD
} ps2_device_t;

void sendbyte(int port, uint8_t byte);

uint8_t readbyte();

void ps2_enable_int();

bool init_ps2();

bool reset_ps2();

void ps2_sanity_check();

int ps2_count();

ps2_device_t ps2_id(int port);

#define ISACK(byte) ((byte)==0xFA)

void ps2_install_callback(int port, void (*callback)(uint8_t byte));

#endif
