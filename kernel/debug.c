/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "common.h"
#include "debug.h"
#include <stdio.h>
#include <console.h>

bool debugging=false;

void enableDebugging() {
	debugging=true;
}

void disableDebugging() {
	debugging=false;
}

void error(string s, ...) {
	int fg=getFGColor();
	if(getFGColor()==COLOR_LRED||getBGColor()==COLOR_LRED) {
		setWriteColor(COLOR_BLACK,getBGColor());
	} else {
		setWriteColor(COLOR_LRED,getBGColor());
	}
	va_list var;
	va_start(var, s);
	string buf=(string)malloc(256);
	vsprintf(buf, s, var);
	puts(buf);
	va_end(var);
	free(buf);
	setWriteColor(fg,getBGColor());
}

void debug(string s, ...) {
	if (!debugging)return;
	va_list var;
	va_start(var, s);
	string buf=(string)malloc(256);
	vsprintf(buf, s, var);
	puts(buf);
	free(buf);
	va_end(var);
}
