/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#ifndef __STACK_H
#define __STACK_H
#include <stdint.h>
#include <stdbool.h>

struct stack;
typedef struct stack * STACK;

STACK createStack(uint32_t size);

void push(STACK s,int32_t val);

int32_t peek(STACK s);

int32_t pop(STACK s);

void roll(STACK s);

void clearStack(STACK s);

bool empty(STACK s);

bool full(STACK s);

#endif
