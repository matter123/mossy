/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __STRING_H
#define __STRING_H
#include <stdint.h>

typedef char * string;

typedef const char * c_string;

int strcmp(c_string str1, c_string str2);

string strcpy(string dest, c_string src);

string strcat(string dest, c_string src);

int strlen(c_string src);

int findstr(string str, string find);

void memcpy(uint8_t *dest, const uint8_t *src, uint32_t len);

void memset(uint8_t *dest, uint8_t val, uint32_t len);

void memset16(uint16_t *dest, uint16_t val, uint32_t len);

void memset32(uint32_t *dest, uint32_t val, uint32_t len);

void memmove(void *dest, const void * src,uint32_t n);

string substr(c_string base, string result, int start, int end);

string strstr(string find, string str);



#endif
