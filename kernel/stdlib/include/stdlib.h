/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __STDLIB_H
#define	__STDLIB_H
#include <string.h>
#include <float.h>
#include <stddef.h>

//#define NULL 0

int atoi(string value);

long atol(string value);

double atof(string value);

string itoa(int value, string str, int radix);

#define RAND_MAX 1073741824

typedef int ErrorCode;
#define ErrorUnknown	-1

int rand();

void init_stdlib();

void srand(int seed);

void * malloc(size_t size);

void * calloc(int num,size_t size);

void * realloc(void *ptr,size_t size);

void free(void * ptr);

#endif
