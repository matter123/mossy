/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __MATH_H
#define __MATH_H
/* kmath.h subset of math.h for kernal use 
   kmath.h and kmath.c are free for public use */
#include <float.h>
#include <stdint.h>
#include <stdbool.h>

int abs(int a);

float fabs(float a);

int pow(int a, int b);

//inline functions

inline float floor(float a) {
    return (int) a;
}

inline float ceil(float a) {
    return ((int) a) == a ? a : ((int) a) + 1;
}

inline int sign(int a) {
    return a < 0 ? -1 : 1;
}

inline int fsign(float a) {
    return a < 0 ? -1 : 1;
}

inline int max(int a, int b) {
    return a < b ? b : a;
}

inline int min(int a, int b) {
    return a < b ? a : b;
}

inline bool oreq(int test, int a, int b) {
    return test == a ? true : test == b ? true : false;
}

uint64_t factorial(uint32_t n);

//typedefs
typedef struct {
	uint32_t X;
	uint32_t Y;
} POINT;

#endif
