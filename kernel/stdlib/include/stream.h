/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#ifndef __STREAM_H
#define __STREAM_H

#include <string.h>
#include <stdint.h>

struct stream;
typedef struct stream * STREAM;

STREAM createStream(uint8_t *mem,uint32_t size);

STREAM copyStream(STREAM s);

uint32_t readU32(STREAM stream);

uint16_t readU16(STREAM stream);

uint8_t readU8(STREAM stream);

int32_t read32(STREAM stream);

int16_t read16(STREAM stream);

int8_t read8(STREAM stream);

uint8_t * readX(STREAM stream,int size);

uint32_t bytesleft(STREAM stream);

void seek(STREAM stream,uint32_t seekpos);

uint32_t seekpos(STREAM stream);

uint32_t streamLength(STREAM stream);

#endif
