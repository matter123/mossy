/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#ifndef __CTYPE_H
#define __CTYPE_H

int isalnum(char c);

int isalpha(char c);

int isctrl(char c);

int isdigit(char c);

int isgraph(char c);

int isprint(char c);

int ispunct(char c);

int isspace(char c);

int isupper(char c);

int isxdigit(char c);

int toupper(char c);

int tolower(char c);

#endif
