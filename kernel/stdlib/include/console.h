/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __CONSOLE_H
#define	__CONSOLE_H

#include <stdint.h>
#include <stdbool.h>
#include <kmath.h>

#define COLOR_BLACK    0x0
#define COLOR_BLUE     0x1
#define COLOR_GREEN    0x2
#define COLOR_CYAN     0x3
#define COLOR_RED      0x4
#define COLOR_MAGENTA  0x5
#define COLOR_BROWN    0x6
#define COLOR_LGRAY    0x7

#define COLOR_GRAY     0x8
#define COLOR_LBLUE    0x9
#define COLOR_LGREEN   0xA
#define COLOR_LCYAN    0xB
#define COLOR_LRED     0xC
#define COLOR_LMAGENTA 0xD
#define COLOR_YELLOW   0xE
#define COLOR_WHITE    0xF

/*
 * Bit 0 - read only, does the console accept input
 *
 * Bit 1 - cooked or raw input handling, set cooked, unset raw
 *
 * Bit 2 - cursor enabled or disabled
 *
 * Bit 3 - serial or grpahical screen
 *
 * Bits 4-14 reserved, DO NOT TOUCH, INTERNAL USE
 *
 * Bit 15 - write only - resets console
 */

void init_console();

void clearScreen();

void clearScreenColor(int BColor);

void setWriteColor(int FG, int BG);

POINT getCursorPos();

void setCursorPos(POINT p);

void clearBG(int color);

void setFlags(uint16_t flags);

int getFGColor();

int getBGColor();

int getFGColorAt(POINT p);

int getBGColorAt(POINT p);

void setWritePos(POINT p);

POINT getWritePos();

uint16_t getFlags();

#endif

