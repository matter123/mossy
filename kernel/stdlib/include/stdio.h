/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __STDIO_H
#define	__STDIO_H
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include "../../common.h"



void printf(string format, ...) __attribute__ ((format (printf, 1, 2)));

void putchar(char c);

int no_optimize vsprintf(string s, string format, va_list arg);

void puts(c_string s);

char getchar();

string gets(string s,int max);

void unputc();

void init_stdio();
#endif

