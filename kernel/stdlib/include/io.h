/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

//collection of methods for using and manipulating IO ports
#include <stdint.h>
#include "../../common.h"

inline void outb(uint16_t port, uint8_t val) {
    asm volatile("outb %0, %1"::"a"(val), "Nd"(port));
}

void outdated outportb(uint16_t port,uint8_t val);

inline void outw(uint16_t port, uint16_t val) {
    asm volatile("outw %0, %1"::"a"(val), "Nd"(port));
}

void outdated outportw(uint16_t port,uint16_t val);

inline void outl(uint16_t port, uint32_t val) {
    asm volatile("outl %0, %1"::"a"(val), "Nd"(port));
}

void outdated outportl(uint16_t port,uint32_t val);

inline uint8_t inb(uint16_t port) {
    uint8_t ret;
    asm volatile("inb %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}

uint8_t outdated inportb(uint16_t port);

inline uint16_t inw(uint16_t port) {
    uint16_t ret;
    asm volatile("inw %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}

uint16_t outdated inportw(uint16_t port);

inline uint32_t inl(uint16_t port) {
    uint32_t ret;
    asm volatile("inl %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}

uint32_t outdated inportl(uint16_t port);

inline void iowait() {
    //port 80 is error checking in POST
    //linux uses this port for iowait;
    //seems to be safe
    asm volatile("outb %%al, $0x80" : : "a"(0));
}