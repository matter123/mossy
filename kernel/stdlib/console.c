/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include <console.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <io.h>
#include "../x86/key_trans.h"
#include "../x86/VGA.h"
#include "../temp/font.h"

#define DEF_FLAGS (uint16_t)(1<<0)|(1<<2)
uint16_t flags=DEF_FLAGS;
static uint8_t drawcolor=COLOR_WHITE;
POINT write;
POINT cursor;
uint8_t *mon=(uint8_t*)0xA0000;

static void putch(POINT p,char c);

volatile uint8_t status=0;
volatile char buf;
void Add(uint8_t letter) {
	status|=1;
	buf=(char)letter;
}

void init_console() {
	install_key_trans_callback(&Add);
	//clearScreen();
	setFlags(DEF_FLAGS);
}

void clearScreen() {
	clearScreenColor(drawcolor>>4);
}

void clearScreenColor(int BColor) {
	/*
	//memset16(mon,(drawcolor&0xF)|(BColor<<4),80*25);
	for(int x=0;x<80;x++) {
		for(int y=0;y<25;y++) {
			mon[80*y+x]=((drawcolor&0xF)|(BColor<<4))<<8|0x20;
		}
	}*/
	write.X=0;
	write.Y=0;
}

void setWriteColor(int FG, int BG) {
	drawcolor=(FG&0xF)|((BG&0xF)<<4);
}

POINT getCursorPos() {
	return cursor;
}

void setCursorPos(POINT p) {
	cursor=p;

	//vga register programming
	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t)((80*p.Y+p.X)&0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t)(((80*p.Y+p.X)>>8)&0xFF));
}

void clearBG(int color) {
	/*
	for(int x=0;x<80;x++) {
		for(int y=0;y<25;y++) {
			mon[80*y+x]=(mon[80*y+x]&0x0FFF)|(color<<12);
		}
	}*/
}

static void scroll() {
	/*
	for (int i=0; i<23; i++) {
			memcpy((uint8_t *)(0xB8000+160*i), (uint8_t *)(0xB8000+160*(i+1)), 160);
	}
	memset16((uint16_t *)(0xB8000+160*23), drawcolor<<8|0x20, 160);*/
}

int getFGColor() {
	return drawcolor&0xF;
}

int getBGColor() {
	return drawcolor>>4;
}

int getFGColorAt(POINT p) {
	return (mon[80*p.Y+p.X]>>8)&0xF;
}

int getBGColorAt(POINT p) {
	return mon[80*p.Y+p.X]>>12;

}

void setWritePos(POINT p) {
	write=p;
}

POINT getWritePos() {
	return write;
}

void setFlags(uint16_t setflags) {
	flags=setflags;
	if(flags&(1<<15)) {
		setFlags(DEF_FLAGS);
		return;
	}

	if(flags&(1<<2)) {
		//enable_cursor();
	}
	else {
		//disable_cursor();
	}

	//add more for any instant change flags
}

uint16_t getFlags() {
	return flags;
}

void puts(c_string s) {
	uint32_t i=0;
	while(*(s+i))putchar(*(s+i++));
}

static char last=0;

void putchar(char c) {
	if(!(flags&(1<<3))) {
		if(c=='\n')putch(write,'\r');
		putch(write,c);
		return;
	}
	last=c;
	if(c>=32) {
		putch(write,c);
		write.X++;
	}
	if(c=='\t')write.X=(write.X+4)&(~3);
	if(c==0x08)write.X--;
	if(c=='\r')write.X=0;
	if(c=='\n'){write.X=0;write.Y++;}

	if(write.X==40) {
		write.Y++;
		write.X=0;
	}
	if(write.Y==25) {
		scroll();
		write.Y=23;
	}
}

void unputc() {
	if(last>=32) {
		write.X--;
	}
	if(last==0x08)write.X++;
	if(write.X>40) { //overflow
			write.Y--;
			write.X=39;
	}
	putch(write,' ');
}

#define PORT 0x3f8   /* COM1 */

int is_transmit_empty() {
   return inb(PORT + 5) & 0x20;
}

void write_serial(char a) {
   while (is_transmit_empty() == 0);
 
   outb(PORT,a);
}


static void putch(POINT p,char c){	
	if(flags&(1<<3)) {
		uint8_t *let=get_char(c);
		for(int row=0;row<8;row++) {
			for(int a=0;a<8;a++) {
				if(let[row]&(1<<a)) {
					int x=p.X*8+a;
					int y=p.Y*8+row;
					mon[y*320+x]=0x3F;	
				}	
			}
		}
	}else {
		write_serial(c);
	}
}

static char read() {
	status&=~(1<<0);
	return buf;
}

char getchar() {
	//raw mode
	if(!(flags&(1<<1))) {
		while(!(status&1<<0));
		return read();
	}
	//cooked mode not implemented yet
	return 0;
}

string gets(string s,int max) {
	//raw mode
	if(!(flags&(1<<1))) {
		int gscount=0;
		char next;
		do {
			next=getchar();
			*(s+gscount)=next;
			}while(next!='\n'&&(gscount++)!=max);
		return s;
	}else {
		return s;
	}
}
