/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include <kmath.h>

int abs(int a) {
	return sign(a)*a;
}

float fabs(float a) {
	return fsign(a)*a;
}

int pow(int a, int b) {
	if (a<=0||b<0)return 0;
	int accum=a;
	for(int i=1;i<b;i++) {
		accum*=a;
	}
	return accum;
}

uint64_t factorial(uint32_t n) {
	if(n<2)return 1;
	return n*factorial(n-1);
}
