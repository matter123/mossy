/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <io.h>
#include "../mem.h"

int atoi(string s) {
	return (int)atol(s);
}

long atol(string s) {
	long temp=0;
	char sign = '+';
	while ( isspace( *s ) ) ++s;
	if ( *s == '+' ) ++s;
	else if ( *s == '-' ) sign = *(s++);
	while(*s) {
		if(isdigit(*s)) {
			temp=temp*10+(*s-'0');
		}
		s++;
	}
	return ( sign == '+' ) ? temp : -temp;
}

//double atof(string value);
static string chars = "ZYXWVUTSRQPONMLKJIHGFEDCBA9876543210123456789ABCDEFGHIJKLMNOPRSTUVWXYZ";

void init_stdlib() {
	init_stdio();
	init_malloc();

	//seed random number gen
	uint32_t seed=0;

	outb(0x70,0x00);
	seed|=inb(0x71)<<0; //seconds

	outb(0x70,0x02);
	seed|=inb(0x71)<<6; //minutes

	outb(0x70,0x04);
	seed|=inb(0x71)<<12; //hours

	outb(0x70,0x06);
	seed|=inb(0x71)<<17; //weekday

	outb(0x70,0x07);
	seed|=inb(0x71)<<20; //day of month

	outb(0x70,0x08);
	seed|=inb(0x71)<<25; //month

	outb(0x70,0x09);
	seed|=(inb(0x71)&7)<<29; //year (truncated)
	srand(seed);
}

void reverse(char* str, int length){
    int i = 0, j = length-1;
    char tmp;
    while (i < j) {
        tmp = str[i];
        str[i] = str[j];
        str[j] = tmp;
        i++; j--;
    }
}
string itoa(int value, string str,int base) {
	string rc;
	string ptr;
	string low;
	// Check for supported base.
	if ( base < 2 || base > 36 ) {
		*str = '\0';
		return str;
	}
	rc = ptr = str;
	// Set '-' for negative decimals.
	if ( value < 0 && base == 10 ) {
		*ptr++ = '-';
	}
	low = ptr;
	// The actual conversion.
	do {
		//using memory to save cpu speed by mirroring array
		*ptr++ = chars[35 + value % base];
		value /= base;
	} while ( value );
	*ptr-- = '\0';
	//reverse modulus trick
	while ( low < ptr ) {
		char tmp = *low;
		*low++ = *ptr;
		*ptr-- = tmp;
	}
	return rc;
}
