/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <stdint.h>
#include <io.h>

void outdated outportb(uint16_t port,uint8_t val) {
	outb(port,val);
}

void outdated outportw(uint16_t port,uint16_t val) {
	outw(port,val);
}

void outdated outportl(uint16_t port,uint32_t val) {
	outl(port,val);
}

uint8_t outdated inportb(uint16_t port) {
	return inb(port);
}

uint16_t outdated inportw(uint16_t port) {
	return inw(port);
}

uint32_t outdated inportl(uint16_t port) {
	return inl(port);
}