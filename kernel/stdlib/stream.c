/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include <stream.h>
#include "../mem.h"

struct stream {
	uint8_t *mem;
	uint32_t seek;
	uint32_t size;
};

STREAM createStream(uint8_t *mem,uint32_t size) {
	STREAM res=(STREAM)malloc(sizeof(struct stream));
	res->mem=mem;
	res->seek=0;
	res->size=size;
	return res;
}

STREAM copyStream(STREAM s) {
	STREAM res=(STREAM)malloc(sizeof(struct stream));
	res->mem=s->mem;
	res->seek=s->seek;
	res->size=s->size;
	return res;
}

void distroyStream(STREAM s) {
	free(s);
}

uint32_t readU32(STREAM stream) {
	uint32_t res=0;
	for(unsigned int i=0;i<sizeof(res);i++) {
		res|=*(stream->mem+i+stream->seek)<<(i*8);
	}
	stream->seek+=sizeof(res);
	return res;
}

uint16_t readU16(STREAM stream){
	uint16_t res=0;
	for(unsigned int i=0;i<sizeof(res);i++) {
		res|=*(stream->mem+i+stream->seek)<<(i*8);
	}
	stream->seek+=sizeof(res);
	return res;
}

uint8_t readU8(STREAM stream) {
	uint8_t res=0;
	for(unsigned int i=0;i<sizeof(res);i++) {
		res|=*(stream->mem+i+stream->seek)<<(i*8);
	}
	stream->seek+=sizeof(res);
	return res;
}

int32_t read32(STREAM stream) {
	int32_t res=0;
	for(unsigned int i=0;i<sizeof(res);i++) {
		res|=*(stream->mem+i+stream->seek)<<(i*8);
	}
	stream->seek+=sizeof(res);
	return res;
}

int16_t read16(STREAM stream) {
	int16_t res=0;
	for(unsigned int i=0;i<sizeof(res);i++) {
		res|=*(stream->mem+i+stream->seek)<<(i*8);
	}
	stream->seek+=sizeof(res);
	return res;
}

int8_t read8(STREAM stream) {
	int8_t res=0;
	for(unsigned int i=0;i<sizeof(res);i++) {
		res|=*(stream->mem+i+stream->seek)<<(i*8);
	}
	stream->seek+=sizeof(res);
	return res;
}

uint8_t * readX(STREAM stream,int size) {
	uint8_t *res=stream->mem+stream->seek;
	stream->seek+=size;
	return res;
}

uint32_t bytesleft(STREAM stream) {
	return stream->size-stream->seek;
}

void seek(STREAM stream,uint32_t seekpos) {
	stream->seek=seekpos;
}

uint32_t seekpos(STREAM stream) {
	return stream->seek;
}

uint32_t streamLength(STREAM stream) {
	return stream->size;
}
