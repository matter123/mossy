/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "../common.h"

//no where near as powerful as a real vsprintf
//but still nice to have
//should be compatible with a regular vsprintf but, behavior is non standard
//no optimize is so that gcc dosn't attempt to mess with anything causing nasty bugs
int no_optimize vsprintf(string s, string format, va_list arg) {
	volatile bool special=false;
	char cn;
	int i;
	double d;

	unsigned int u;
	string buf=(string)malloc(65);
	while (*format) {
		char c=*format++;
		if (special) {
			switch (c) {
				case 'a':
					d=va_arg(arg,double); //not to break compatibly
					break;
				case 'A':
					d=va_arg(arg,double); //same as above
					break;
				case 'c':
					cn=(char)va_arg(arg,int);
					*s++=cn;
					break;
				case 'd':
				case 'i':
					i=va_arg(arg,int);
					itoa(i,buf,10);
					while(*buf)*s++=*buf++;
					break;
				case 'e':
				case 'E':
				case 'f':
				case 'F':
				case 'g':
				case 'G':
					d=va_arg(arg,double); //see a
					break;
				case 'n':
					*(va_arg(arg,int *))=(int)d; //not implemented
					break;
				case 'o':
					u=va_arg(arg,unsigned int);
					itoa(u,buf,8);
					while(*buf)*s++=*buf++;
					break;
				case 'p':
					itoa(va_arg(arg,int *),buf,16);
					while(*buf)*s++=*buf++;
					break;
				case 's':
					buf=va_arg(arg,string);
					while(*buf)*s++=*buf++;
					break;
				case 'u':
					u=va_arg(arg,unsigned int);
					itoa(u,buf,10);
					while(*buf)*s++=*buf++;
					break;
				case 'x':
				case 'X':
					u=va_arg(arg,unsigned int);
					itoa(u,buf,16);
					*s++='0';
					*s++='x';
					while(*buf)*s++=*buf++;
					break;
			}
			special=false;
		} else {
			if (c=='%') {
				special=true;
			} else {
				*s++=c;
			}
		}
	}
	*s='\0';
	free(buf);
	return strlen(s);
}
