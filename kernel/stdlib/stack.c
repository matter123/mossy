/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include <stack.h>
#include "../mem.h"

struct stack {
	uint32_t current;
	uint32_t size;
	int32_t *stack;
};

STACK createStack(uint32_t size) {
	STACK s=(STACK)malloc(sizeof(struct stack));
	s->size=size;
	s->current=0;
	s->stack=(int32_t *)malloc(size*4);
	return s;

}

void distroyStack(STACK s) {
	free(s->stack);
	free(s);
}

void push(STACK s,int32_t val) {
	if(full(s))return;
	*(s->stack+s->current++)=val;
}

int32_t peek(STACK s) {
	if(empty(s))return 0;
	return *(s->stack+s->current-1);
}

int32_t pop(STACK s) {
	if(empty(s))return 0;
	return *(s->stack+ --(s->current));
}

void roll(STACK s) {
	int32_t temp1=pop(s);
	int32_t temp2=pop(s);
	push(s,temp1);
	push(s,temp2);
}

void clearStack(STACK s) {
	while(!empty(s))pop(s);
}

bool full(STACK s) {
	return s->current==s->size;
}

bool empty(STACK s) {
	return s->current==0;
}
