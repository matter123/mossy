/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include <string.h>


//from PDCLIB
int strcmp(c_string str1, c_string str2) {
	while (*str1 && *str1 == *str2 ) {
		++str1;
		++str2;
	}
	return *str1 - *str2;
}

string strcpy(string dest, c_string src) {
	while (*src)*(dest++)=*(src++);
	*dest=0;
	return dest;
}

string strcat(string dest, c_string src) {
	while (*dest)*dest=*(dest++);
	while (*src)*(dest++)=*(src++);
	*dest=0;
	return dest;
}

int strlen(c_string src) {
	int i=0;
	while (*(src)++)i++;
	return i;
}

//from nucleode https://bitbucket.org/TheBenjaneer/libnucleode, used with premission
//the following function is under modified BSD 3 license
int findstr(string str, string find) {
	int str_len=strlen(str);
	int find_len=strlen(find);
	if(str_len < find_len) {
		return -2;	
	}
	int f1=0;
	int f2=1;
	int i, j;
	for(i=0;i<str_len;i++) {
		if(str[i] == find[0] && ((int)str_len-i) >= find_len ) {
			f1=1;
			f2=1;
			for(j=0;j<find_len;j++) {
				if (str[i+j]!=find[j]) {
					f2 = 0;
					break;
				}
			}
			if(f1==1&&f2==1) {
				return i;
			}
		}
    }
	if(f1==1&&f2==1) {
		return i;
	}
	return -1;
}

//memset and memcopy in mem.h

void memmove(void *dest, const void * src,uint32_t n) {
	int delta=0;
	uint32_t from=0;
	uint32_t to=0;
	if(dest==src)return;
	if(dest<src) {
		delta=1;
		from=0;
		to=n;
	}else {
		delta=-1;
		from=n-1;
		to =-1;
	}
	for(uint32_t offset=from;offset!=to;offset+=delta)
		*(((uint8_t*)dest) + offset) = *(((uint8_t*)src) + offset);
}


string substr(c_string base, string result, int start, int end) {
	if(end==0)end=strlen(base);
	int len=end-start;
	for(int i=0;i<len;i++) {
		*(result++)=*(base+start+i);
	}
	*(result)='\0';
	return result;
}

string strstr(string find, string str) {
	int index=findstr(str,find);
	if(index>=0)return str;
	return (str+index);
}
