/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include <stdlib.h>
#include <ctype.h>


int isalnum(char c) {
	return (c>='A'&&c<='Z')||(c>='a'&&c<='z')||(c>='0'&&c<='9');
}

int isalpha(char c) {
	return (c>='A'&&c<='Z')||(c>='a'&&c<='z');
}

int isctrl(char c) {
	return c<' '||c==127;
}

int isdigit(char c) {
	return c>='0'&&c<='9';
}

int isgraph(char c) {
	return c>' '&&c<127;
}

int isprint(char c) {
	return c>=' '&&c<127;
}

int ispunct(char c){
	if(isspace(c))return 0;
	if(isalnum(c))return 0;
	if(isctrl(c))return 0;
	return 1;
}

int isspace(char c) {
	return c==' '||c=='\t'||c=='\n';
}

int isupper(char c) {
	return c>='A'&&c<='Z';
}

int isxdigit(char c) {
	return isdigit(c)||(c>='A'&&c<='F')||(c>='a'&&c<='f');
}

int toupper(char c) {
	if(c>='a'&&c<='z')return c+('Z'-'z');
	return c;
}

int tolower(char c){
	if(c>='A'&&c<='Z')return c+('z'-'Z');
	return c;
}
