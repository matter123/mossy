/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#ifndef __MEM_H
#define __MEM_H
#include "common.h"
#include <string.h>

typedef struct mmap {
    uint64_t addr;
    uint64_t length;
    uint32_t type;
} mmap_t;
uint32_t kernel_end_addr();

uint32_t kernel_size();

void init_mem(uint32_t mmapAddr, int length,uint32_t startaddr,uint32_t upmem);

int mem_region_count();

mmap_t * mem_region(int region);

uint32_t memsearch_j(uint32_t start, uint32_t end, uint32_t jump, string str);
uint32_t memsearch(uint32_t start, uint32_t end, string str);

uint32_t k_alloc_addr();

void * k_alloc_pa(uint32_t size);

void malloc_test();

#endif
