/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "VGA.h"

VBE_info_t *vbeinfo;
VBE_mode_t *curmode;

void init_VGA(multiboot_t *mboot) {
	vbeinfo=(VBE_info_t *)mboot->vbe_control_info;
	curmode=(VBE_mode_t *)mboot->vbe_mode_info;
	return;
}

uint32_t total_vga_mem() {
	return vbeinfo->totalMem*64;
}

int disp_width() {
	return curmode->XRes;
}

int disp_height() {
	return curmode->YRes;
}

int disp_depth() {
	return curmode->bpp;
}

VBE_mode_t *current_mode() {
	return curmode;
}
