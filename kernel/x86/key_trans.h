/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __KEY_TRANS_H
#define __KEY_TRANS_H
#include "../common.h"

void install_key_trans_callback(void (*callback)(uint8_t letter));

void init_key_trans();

bool ismodKeyPressed(int modKey);

bool isCapslockActive();

bool isShiftPressed();

bool isCtrlPressed();

bool isAltPressed();

#endif

