/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "pit.h"
#include "idt.h"
#include <io.h>
#define PIT_FREQ 1193180
volatile uint64_t tickcount=0;
uint32_t freqtc=0;
uint32_t counter=0;
int sec=0;
static void IRQ_0(registers_t reg);

void init_pit(int freq) {
	debug("setting up pit\n");
	freqtc=freq;
	counter=0;
	tickcount=0;
	irq_install_handler(0, &IRQ_0);
	uint32_t divisor32=PIT_FREQ/freq;
	if (divisor32>65535)divisor32=65535;
	uint16_t divisor=(uint16_t)divisor32;
	outb(0x43, 0x36);
	uint8_t lower=(uint8_t)(divisor&0xFF);
	uint8_t upper=(uint8_t)((divisor>>8) & 0xFF);
	outb(0x40, lower);
	outb(0x40, upper);
}

static void IRQ_0(registers_t reg) {
	tickcount++;
	counter++;
	if(counter==freqtc) {
		counter=0;
		sec++;
	}
}

uint64_t get_tick_count() {
	return tickcount;
}

//get the approx seconds since boot time 
int get_seconds() {
	return sec;
}
