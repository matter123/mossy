/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "../common.h"
#include "idt.h"
#include "irq.h"
#include <kmath.h>
#include <io.h>
#include "../debug.h"

#define ICW1_ICW4       0x01		/* ICW4 (not) needed */

#define ICW1_INIT       0x10		/* Initialization - required! */

#define ICW4_8086       0x01		/* 8086/88 (MCS-80/85) mode */

void irq_remap();

void *irq_func[16]={
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};

uint16_t get_isr() {
	outb(PIC1_COMMAND, 0x0b);
	iowait();
	outb(PIC2_COMMAND, 0x0b);
	iowait();
	return (inb(PIC2_COMMAND)<<8)|inb(PIC1_COMMAND);
}

void irq_handler(registers_t reg) {
	//hand spurious interrupts
	if (oreq(reg.int_num, 7, 15)) {
		debug("\nSpurious interrupt %d", reg.int_num);
		if (!(get_isr()&(1<<7)))return;
		if (!(get_isr()&(1<<15))) {
			outb(PIC1_COMMAND, 0x20); //master still requires EOI its real to it
			return;
		}
	}
	if (reg.int_num==15) {
	}
	if (!irq_func[reg.int_num]) {
		debug("\nIRQ:\t%d\n",reg.int_num);
	} else {
		void (*handler)(registers_t r)
				=irq_func[reg.int_num];
		handler(reg);
	}
	if (reg.int_num>7) {
		// Send reset signal to slave.
		outb(PIC2_COMMAND, 0x20);
	}
	// Send reset signal to master. (As well as slave, if necessary).
	outb(PIC1_COMMAND, 0x20);
}

void init_irq() {
	debug("initializing irqs\n");
	irq_remap(0x20, 0x28); //change PIC1 from 8-15 to 32-39
	idt_set_gate(32, (uint32_t)&irq0, 0x08, 0x8E);
	idt_set_gate(33, (uint32_t)&irq1, 0x08, 0x8E);
	idt_set_gate(34, (uint32_t)&irq2, 0x08, 0x8E);
	idt_set_gate(35, (uint32_t)&irq3, 0x08, 0x8E);
	idt_set_gate(36, (uint32_t)&irq4, 0x08, 0x8E);
	idt_set_gate(37, (uint32_t)&irq5, 0x08, 0x8E);
	idt_set_gate(38, (uint32_t)&irq6, 0x08, 0x8E);
	idt_set_gate(39, (uint32_t)&irq7, 0x08, 0x8E);
	idt_set_gate(40, (uint32_t)&irq8, 0x08, 0x8E);
	idt_set_gate(41, (uint32_t)&irq9, 0x08, 0x8E);
	idt_set_gate(42, (uint32_t)&irq10, 0x08, 0x8E);
	idt_set_gate(43, (uint32_t)&irq11, 0x08, 0x8E);
	idt_set_gate(44, (uint32_t)&irq12, 0x08, 0x8E);
	idt_set_gate(45, (uint32_t)&irq13, 0x08, 0x8E);
	idt_set_gate(46, (uint32_t)&irq14, 0x08, 0x8E);
	idt_set_gate(47, (uint32_t)&irq15, 0x08, 0x8E);
}

/*the PIC needs to be remapped because IBM used
 *  reserved exceptions and that conflicts with
 *  exceptions that are in use now so the remap*/
void irq_remap(uint32_t offset1, uint32_t offset2) {
	debug("remaping irq controller\n");
	outb(PIC1_COMMAND, ICW1_INIT+ICW1_ICW4); //reset pic
	iowait();
	outb(PIC2_COMMAND, ICW1_INIT+ICW1_ICW4);
	iowait();
	outb(PIC1_DATA, offset1); //setup offsets
	iowait();
	outb(PIC2_DATA, offset2);
	iowait();
	outb(PIC1_DATA, 0x04); //setup IRQ2 as a cascade
	iowait();
	outb(PIC2_DATA, 0x02); //tell PIC2 its on a cascade
	iowait();
	outb(PIC1_DATA, ICW4_8086); //x86 mode
	iowait();
	outb(PIC2_DATA, ICW4_8086);
	iowait();

	outb(PIC1_DATA, 0x0); //enable all intterupts
	outb(PIC2_DATA, 0x0);
}

void irq_install_handler(int irq, void (*handler)(registers_t r)) {
	irq_func[irq]=handler;
}

void irq_unistall_handler(int irq) {
	irq_func[irq]=0;
}
