/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __KEYCODES_H
#define __KEYCODES_H

uint16_t unknown = 0x6D;
#define UNKN 0x6D
//large swaths of translations
//blame IBM for randomness
//examples are for us qwerty
//MSB is released
//AND with 0x7FFF before using keycode
#define NCDE 0x00
uint16_t keycodes[1023] = {
    NCDE, 0x2D, 0x2B, 0x29, //NCDE, F9  , F7, F5 //note my keyboard produces scancode 0x2 when pressing F7
    0x27, 0x25, 0x26, 0x30, //F3  , F1  , F2  , F12
    NCDE, 0x2E, 0x2C, 0x2A, //NCDE, F10 , F8  , F6
    0x28, 0x32, 0x39, NCDE, //F4  , TAB , TICK, NCDE
    NCDE, 0x47, 0x45, NCDE, //NCDE, LALT, LSHI, NCDE
    0x4C, 0x11, 0x1C, NCDE, //LCTR, Q   , 1   , NCDE
    NCDE, NCDE, 0x1A, 0x13, //NCDE, NCDE, Z   , S
    0x01, 0x17, 0x1D, NCDE, //A   , W   , 2   , NCDE
    NCDE, 0x03, 0x18, 0x04, //NCDE, C   , X   , D
    0x05, 0x1F, 0x1E, NCDE, //E   , 4   , 3   , NCDE
    NCDE, 0x4E, 0x16, 0x06, //NCDE, SPCE, V   , F
    0x14, 0x12, 0x20, NCDE, //T   , R   , 5   , NCDE
    NCDE, 0x0E, 0x02, 0x08, //NCDE, N   , B   , H
    0x07, 0x19, 0x21, NCDE, //G   , Y   , 6   , NCDE
    NCDE, NCDE, 0x0D, 0x0A, //NCDE, NCDE, M   , J
    0x15, 0x22, 0x23, NCDE, //U   , 7   , 8   , NCDE
    NCDE, 0x41, 0x0B, 0x09, //NCDE, ',' , K   , I
    0x0F, 0x1B, 0x24, NCDE, //O   , 0   , 9   , NCDE
    NCDE, 0x42, 0x43, 0x0C, //NCDE, .   , /   , L
    0x3F, 0x10, 0x3A, NCDE, //;   , P   , -   , NCDE
    NCDE, NCDE, 0x40, NCDE, //NCDE, NCDE, '   , NCDE
    0x3C, 0x3B, NCDE, NCDE, //[   , =   , NCDE, NCDE
    0x44, 0x4B, 0x33, 0x3D, //CAPS, RSHI, ENT , ]
    NCDE, 0x3E, NCDE, NCDE, //NCDE, \   , NCDE, NCDE
    NCDE, NCDE, NCDE, NCDE, //not in set 2 according to osdev, but its needed
    NCDE, NCDE, 0x31, NCDE, //NCDE, NCDE, BKSP, NCDE
    NCDE, 0x61, NCDE, 0x64, //NCDE, K1  , NCDE, K4
    0x67, NCDE, NCDE, NCDE, //K7  , NCDE, NCDE, NCDE
    0x60, 0x5F, 0x62, 0x65, //K0  , KDOT, K2  , K5
    0x66, 0x68, 0x4F, 0x54, //K6  , K8  , ESC , NUM
    0x2F, 0x5D, 0x63, 0x5C, //F11 , KADD, K3  , KSUB
    0x5B, 0x69, 0x52, 0x53, //KMUL, K9  , SCRL, PASE //pause isnt actually here just a nice spot 
    NCDE, NCDE, NCDE, 0x2B, //NCDE, NCDE, NCDE, real F7
    0xFF00, 0xFF00, 0xFF00, 0xFF00, //divider between regular and extended
};
/*
    //extended keys
    0x48 //right alt
    0x4D //right ctrl
    0x46 //left meta
    0x49 //right meta
    0x6A //power
    0x6B //sleep
    0x5A //KDIV
    0x5E //KENT
    0x6C //wake
    0x57 //end
    0x37 //left
    0x56 //home
    0x55 //insert
    0x50 //delete
    0x36 //down
    0x38 //right
    0x35 //up
    0x59 //page down
    0x58 //page up 
 */

void set_extended(uint8_t scancode, uint16_t keycode);

void init_extended_keys() {
    set_extended(0x11, 0x48);
    set_extended(0x14, 0x4D);
    set_extended(0x1F, 0x46);
    set_extended(0x27, 0x49);
    set_extended(0x2F, 0x4A);
    set_extended(0x33, 0x6A);
    set_extended(0x3F, 0x6B);
    set_extended(0x4A, 0x5A);
    set_extended(0x5A, 0x5E);
    set_extended(0x5E, 0x6C);
    set_extended(0x69, 0x57);
    set_extended(0x6B, 0x37);
    set_extended(0x6C, 0x56);
    set_extended(0x70, 0x55);
    set_extended(0x70, 0x50);
    set_extended(0x72, 0x36);
    set_extended(0x74, 0x38);
    set_extended(0x75, 0x35);
    set_extended(0x7A, 0x59);
    set_extended(0x7D, 0x58);
}

void set_extended(uint8_t scancode, uint16_t keycode) {
    keycodes[0x88 + scancode] = keycode;
}
#endif

