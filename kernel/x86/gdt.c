/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "../common.h"
#include "../debug.h"

struct gdt_entry {
	uint16_t limit_low;
	uint16_t base_low;
	uint8_t base_middle;
	uint8_t access;
	uint8_t granularity;
	uint8_t base_high;
} __attribute__((packed));

//has to be packed because its six bytes
struct gdt_ptr {
	uint16_t limit;
	uint32_t base;
} __attribute__((packed));

struct gdt_entry gdt[5];
struct gdt_ptr gp;

//in gdt.s
extern void gdt_flush();

void gdt_set_gate(uint32_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran) {
	gdt[num].base_low=(base&0xFFFF);
	gdt[num].base_middle=(base>>16) & 0xFF;
	gdt[num].base_high=(base>>24) & 0xFF;

	gdt[num].limit_low=(limit&0xFFFF);
	gdt[num].granularity=((limit>>16) & 0x0F);

	gdt[num].granularity|=(gran&0xF0);
	gdt[num].access=access;
}

void init_gdt() {
	debug("initializing gdt\n");
	gp.limit=(sizeof (struct gdt_entry) * 5)-1;
	gp.base=(uint32_t)&gdt;
	gdt_set_gate(0, 0, 0, 0, 0); // Null segment
	gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Code segment
	gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Data segment
	gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // (User) Code segment
	gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // (User) Data segment
	gdt_flush();
}

