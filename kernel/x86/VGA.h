/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __VGA_H
#define __VGA_H
#include "../common.h"
#include "../multiboot.h"

typedef struct packed_s info {
	char     signature[4]; // {'V','E','S','A'}
	uint16_t version; //0x0300 VBE 3.0
	uint32_t OEMstr;
	uint8_t  capabilities[4];
	uint32_t VModeList;
	uint16_t totalMem;
	uint16_t OEM_VENrev;
	uint32_t OEM_VENname;
	uint32_t OEM_PROname;
	uint32_t OEM_PROrev;
	uint8_t  reserved[222];
	//read only
	uint8_t  data_ro[256];
} VBE_info_t;

typedef struct packed_s mode {
	uint16_t attrib;
	uint8_t  win_Aattrib,win_Battrib;
	uint16_t win_Gran;
	uint16_t win_Size;
	uint16_t win_Astart, win_Bstart;
	uint32_t win_Func;
	uint16_t BytesPerScanline;
	
	//VBE 1.2
	uint16_t XRes, YRes;
	uint8_t  XChar, YChar;
	uint8_t  num_Planes;
	
	uint8_t  bpp;
	uint8_t  num_Banks;
	uint8_t  mem_Model;
	uint8_t  bank_Size;
	uint8_t  num_IPages;
	uint8_t  reserved1;
	uint8_t  red_maskSize;
	uint8_t  red_fieldpos;
	uint8_t  green_maskSize;
	uint8_t  green_fieldpos;
	uint8_t  blue_maskSize;
	uint8_t  blue_fieldpos;
	uint8_t  rsvd_maskSize;
	uint8_t  rsvd_fieldpos;
	uint8_t  DC_modeInfo;
	
	//VBE 2.0
	uint32_t physAddr;
	uint32_t ptr_offscrnmem;
	uint16_t amt_offscrnmem;
	uint8_t  reserved[206];
} VBE_mode_t;

void init_VGA(multiboot_t *mboot);

int disp_width();

int disp_height();

int disp_depth();

uint32_t total_vga_mem();

VBE_mode_t *current_mode();

void disp_cpy_buf_full(void *buf);

void disp_cpy_buf(int x,int y, int width,int height,void *buf);

#endif

