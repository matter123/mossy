/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef  __IRQ_H
#define  __IRQ_H
#include "../common.h"
#include "idt.h"

#define PIC1            0x20		/* IO base address for master PIC */
#define PIC2            0xA0		/* IO base address for slave PIC */
#define PIC1_COMMAND	PIC1
#define PIC1_DATA       (PIC1+1)
#define PIC2_COMMAND	PIC2
#define PIC2_DATA       (PIC2+1)

void irq_handler(registers_t reg);

void init_irq();

void irq_install_handler(int irq, void (*handler)(registers_t r));

void irq_unistall_handler(int irq);

extern void * irq0;
extern void * irq1;
extern void * irq2;
extern void * irq3;
extern void * irq4;
extern void * irq5;
extern void * irq6;
extern void * irq7;
extern void * irq8;
extern void * irq9;
extern void * irq10;
extern void * irq11;
extern void * irq12;
extern void * irq13;
extern void * irq14;
extern void * irq15;

#endif //__IRQ_H
