/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "../speaker.h"
#include "../sleep.h"
#include <io.h>

void enable(int freq);

void disable();

void beep(int ms) {
	beep_freq(1000,ms);
}

void beep_freq(int freq,int ms) {
	enable(freq);
	mSleep(ms);
	disable();
}

void enable(int freq) {
 	uint32_t div = 1193180 / freq;
 	outb(0x43, 0xb6);
 	outb(0x42, (uint8_t) (div) );
 	outb(0x42, (uint8_t) (div >> 8));

 	uint8_t tmp = inb(0x61);
 	outb(0x61, tmp | 3);
}

void disable() {
	uint8_t tmp = inb(0x61);
 	outb(0x61, ~(tmp & 3));
}

void note(int note,int length) {
	beep_freq(note,length);
	mSleep(75);
	
}