/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "../common.h"
#include "isr.h"
#include "irq.h"
#include "../syscall.h"
#include "../panic.h"

/* Defines an IDT entry */
struct idt_entry {
	uint16_t base_lo;
	uint16_t sel;
	uint8_t always0;
	uint8_t flags;
	uint16_t base_hi;
} __attribute__((packed));

struct idt_ptr {
	uint16_t limit;
	uint32_t base;
} __attribute__((packed));

struct idt_entry idt[256];
struct idt_ptr idtp;

extern void idt_load();

void idt_set_gate(uint8_t num, uint32_t base, uint16_t sel, uint8_t flags) {
	idt[num].base_lo=base&0xFFFF;
	idt[num].base_hi=(base>>16) & 0xFFFF;
	idt[num].sel=sel;
	idt[num].always0=0; //ensure
	idt[num].flags=flags;
}

void init_idt() {
	debug("initializing idt\n");
	idtp.limit=(sizeof (struct idt_entry) * 256)-1;
	idtp.base=(uint32_t)&idt;
	memset((uint8_t *)idtp.base, 0, idtp.limit+1);
	init_isr();		//00-31
	init_irq();		//32-47
	init_syscall();	//96-255
	idt_load();
}

void isr_handler(registers_t reg) {
	int in=reg.int_num;
	if (in==0||in==6||in==8||in==13||in==18) {
		//(currently)unrecoverable exceptions
		isr_panic(in, reg.err_code);
	}
	//special treatment
	if(in==14) {
		panic(PANIC_PAGE_FAULT,reg.err_code);
	}
}

