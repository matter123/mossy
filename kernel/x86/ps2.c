/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "../ps2.h"
#include "irq.h"
#include "../sleep.h"
#include "../debug.h"
#include <io.h>

#define PS2_PORT_DATA   0x60
#define PS2_PORT_STATUS 0x64
#define PS2_PORT_CMD    0x64
#define PS2_READ_ERROR  0xEF

uint8_t pol_readbyte();
void sendcmd(uint8_t byte);
void enableDisable(bool enable);
void _sendbyte(uint8_t byte);
void _sendbyte2(uint8_t byte);
void port1(registers_t regs);
void port2(registers_t regs);


//waits up to 50ms for input buffer to clear

bool wait_write() {
	int i=0;
	while ((inb(PS2_PORT_STATUS)&2)/*input buffer has to be clear to write*/) {
		mSleep(1);
		if (i++ >50)return false;
	}
	return true;
}
//waits up to 50ms for output buffer to set

bool wait_read() {
	int i=0;
	while (!(inb(PS2_PORT_STATUS)&1)/*output buffer has to set to read*/) {
		mSleep(1);
		if (i++ >50)return false;
	}
	return true;
}

bool wait_read_long() {
	int i=0;
	while (!(inb(PS2_PORT_STATUS)&1)/*output buffer has to set to read*/) {
		mSleep(10);
		if (i++ >300)return false; //for the reset keyboard can take up to 3 secs
	}
	return true;
}

bool port1_usable;
bool port2_usable;

ps2_device_t device[2];

bool init_ps2() {
	if (!reset_ps2())return false;
	if (port1_usable)device[0]=ps2_id(1);
	if (port2_usable)device[1]=ps2_id(2);
	if (port1_usable)irq_install_handler(1, &port1);
	if (port2_usable)irq_install_handler(12, &port2);
	return true;
}

/*
 *  - - - - - - - - - - - - Configuration byte layout - - - - - - - - - - -
 * 
 *  +---------+---------+-----------+----+---------+---------+---------+----+
 *  |    0    |    1    |     2     |  3 |    4    |    5    |    6    |  7 |
 *  +---------+---------+-----------+----+---------+---------+---------+----+
 *  |prt 1 int|prt 2 int|POST passed|zero|1st clock|2nd clock|kbd trans|zero|
 *  +---------+---------+-----------+----+---------+---------+---------+----+
 *
 */

bool reset_ps2() {
	//TODO check is ps2 exists
	debug("\n");
	port2_usable=true;
	port1_usable=true;

	enableDisable(false); //disable both

	wait_read(); //clear input buffer

	//first set of cfg byte
	sendcmd(0x20);
	uint8_t cfg=pol_readbyte();
	cfg&=(~(1<<0|1<<1|1<<6));
	sendcmd(0x60);
	sendbyte(1, cfg);

	//check if 2 PS2 devices
	sendcmd(0x20);
	port2_usable=pol_readbyte()&(1<<5);
	debug("PS/2 port 2 plugged in %b\n", port2_usable);

	//self test
	sendcmd(0xAA);
	if (pol_readbyte()!=0x55) {
		error("self test failed\n");
		return false;
	}

	//double check port 2 actually exists 
	if (port2_usable) {
		sendcmd(0xA8);
		sendcmd(0x20);
		port2_usable=!(pol_readbyte()&(1<<5));
		if (!port2_usable)debug("port 2 disappeared\n");
		sendcmd(0xA7);
	}

	//interface tests
	sendcmd(0xAB);
	if (pol_readbyte()!=0x00) {
		debug("port 1 failed");
		port1_usable=false;
	}
	if (port2_usable) {
		sendcmd(0xA9);
		if (pol_readbyte()!=0x00) {
			debug("port 2 failed\n");
			port2_usable=false;
		}
	}
	if (!(port1_usable||port2_usable)) {
		error("no ps/2 ports available\n");
		return false;
	}

	enableDisable(true);

	//reset
	if (port1_usable) {
		sendbyte(1, 0xFF);
		pol_readbyte(); //ACK
		wait_read_long();
		uint8_t res=inb(PS2_PORT_DATA);
		if (res!=0xAA) {
			debug("reset failed port 1 errcode %x\n", res);
			port1_usable=false;
		}
	}
	if (port2_usable) {
		sendbyte(2, 0xFF);
		pol_readbyte(); //ACK
		wait_read_long();
		uint8_t res=inb(PS2_PORT_DATA);
		if (res!=0xAA) {
			debug("reset failed port 2 errcode %x\n", res);
			port2_usable=false;
		}
	}
	if (!(port1_usable||port2_usable)) {
		error("no ps/2 ports available\n");
		return false;
	}

	debug("all good\n");
	//all good
	return true;
}

void sendbyte(int port, uint8_t byte) {
	if (--port)_sendbyte2(byte);
	else _sendbyte(byte);
}

uint8_t readbyte() {
	return pol_readbyte();
}

// <editor-fold defaultstate="collapsed" desc="internal methods">

void enableDisable(bool enable) {
	if (!enable) {
		sendcmd(0xAD);
		sendcmd(0xA7);
	} else {
		if (port1_usable)
			sendcmd(0xAE);
		if (port2_usable)
			sendcmd(0xA8);
	}
}

void sendcmd(uint8_t byte) {
	wait_write();
	outb(PS2_PORT_CMD, byte);
}

void _sendbyte(uint8_t byte) {
	if (port1_usable) {
		wait_write();
		outb(PS2_PORT_DATA, byte);
	}
}

void _sendbyte2(uint8_t byte) {
	if (port2_usable) {
		sendcmd(0xD4);
		wait_write();
		outb(PS2_PORT_DATA, byte);
	}
}

uint8_t pol_readbyte() {
	if (wait_read())
		return inb(PS2_PORT_DATA);
	return 0xFF;
}// </editor-fold>

void ps2_enable_int() {
	sendcmd(0x20);
	uint8_t cfg=pol_readbyte();
	cfg|=(1<<0|1<<1);
	sendcmd(0x60);
	sendbyte(1, cfg);
}

int ps2_count() {
	return port1_usable&&port2_usable?2:1;
}

ps2_device_t ps2_id(int port) {
	if (device[port-1])return device[port-1];
	sendbyte(port, 0xF5);
	debug("disable scaning %x\n", pol_readbyte());
	sendbyte(port, 0xF2);
	debug("Identify %x \n", pol_readbyte());
	uint8_t resp[2];
	resp[0]=pol_readbyte();
	resp[1]=pol_readbyte();
	if (resp[0]==0xFA)resp[0]=resp[1];
	switch (resp[0]) {
		case 0x00:
		case 0x03:
		case 0x04:
			return MOUSE;
		case 0xAB:
			return KEYBOARD;
	}
	debug("unknown id %x %x port %d\n", resp[0], resp[1], port);
	return UNKNOWN;
}

volatile void * ps2_callback[2]={0, 0};

void port1(registers_t regs) {
	if (ps2_callback[0]) {
		void (*callback)(uint8_t byte)=ps2_callback[0];
		callback(inb(PS2_PORT_DATA));
	} else
		debug("port 1 intterupt %x\n", inb(PS2_PORT_DATA));
}

void port2(registers_t regs) {
	if (ps2_callback[1]) {
		void (*callback)(uint8_t byte)=ps2_callback[1];
		callback(inb(PS2_PORT_DATA));
	} else
		debug("port 2 intterupt %x\n", inb(PS2_PORT_DATA));
}

void ps2_sanity_check() {
	if (ps2_id(1)!=KEYBOARD&&ps2_id(2)!=KEYBOARD) {
		error("\nno keyboard present, aborting");
		end();
	}
	if (ps2_id(1)==KEYBOARD&&ps2_id(2)==KEYBOARD) {
		error("\ntwo keyboards present, aborting");
		end();
	}
}

void ps2_install_callback(int port, void (*callback)(uint8_t byte)) {
	debug("install ps2 port %d callback", port);
	ps2_callback[port-1]=callback;
}
