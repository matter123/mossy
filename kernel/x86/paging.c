/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "paging.h"
#include "../mem.h"

page_dir_t * cur_dir;

void init_paging() {
	debug("enabling paging\n");
	//identity map all ram
	page_dir_t *page_dir=(page_dir_t *)k_alloc_pa(sizeof(page_dir_t));
	page_table_t *page_table=0;
	void *loc=0x0;
	uint32_t entries=(ramsize()/4)+(ramsize()%4?1:0);
	if(entries%1024)entries+=(1024-(entries%1024));
	for(unsigned int i=0;i<entries;i++) {
		if(i%1024==0) {
			page_table=(page_table_t *)k_alloc_pa(sizeof(page_table_t));
			install_page_table(page_dir,i/1024,page_table,false,true);
		}
		install_page(page_table, i%1024, loc, false, true);
		loc+=0x1000;
	}
	switch_to(page_dir);
	//end();
	enable_paging();
}

void install_page(page_table_t *table,int index, void * physical_addr,bool user,bool writable) {
	uint32_t nphys=(uint32_t)physical_addr;
	if(nphys!=(nphys&0xFFFFF000)){error("page table isn't page aligned");end();}
	table->page_entries[index].addr=nphys>>12;
	table->page_entries[index].resv1=0;
	table->page_entries[index].resv2=0;
	table->page_entries[index].unused=0;
	table->page_entries[index].userread=user;
	table->page_entries[index].writable=writable;
	table->page_entries[index].present=true;
	
}

void install_page_table(page_dir_t * dir,int index,page_table_t * table,bool user, bool writable) {
	debug("installing page %d\n",index);
	uint32_t ntable=(uint32_t)table;
	if(ntable!=(ntable&0xFFFFF000)){error("page table isn't page aligned");end();}
	dir->page_dir_entries[index].addr=ntable>>12;
	dir->page_dir_entries[index].resv1=0;
	dir->page_dir_entries[index].resv2=0;
	dir->page_dir_entries[index].zero=0;
	dir->page_dir_entries[index].userread=user;
	dir->page_dir_entries[index].writable=writable;
	dir->page_dir_entries[index].present=true;
}

void switch_to(page_dir_t * dir) {
	cur_dir=dir;
	asm volatile("mov %0, %%cr3":: "r"(dir));
	flush_tlb();
}

void flush_tlb() {
	int a=0;
	asm volatile("movl %%cr3, %0":"=r"(a));
	asm volatile("movl %0, %%cr3"::"r"(a));
}

void invl_page(int page) {
#if CPU >= 486
	uint32_t addr=cur_dir->page_dir_entries[page].addr;
	asm volatile("invlpg (%0)" ::"r" (addr) : "memory");
#else
	flush_tlb(); //386 cant invalidate a single page
#endif
}

void enable_paging() {
	uint32_t cr0;
	asm volatile("mov %%cr0, %0": "=r"(cr0));
	cr0 |= 0x80000000; // Enable paging!
	asm volatile("mov %0, %%cr0":: "r"(cr0));
}

void ident_map(void *start_ptr,uint32_t size) {
	uint32_t start=(uint32_t)start_ptr;
	start=start&0xFFFFF000;
	start/=4096;
	uint32_t end=start+(size/4);
	if(size%4)end++;
	page_table_t *page_table=0;
	for(int i=start;i<=end;i++) {
		if(cur_dir->page_dir_entries[i/1024].addr!=0) {
			page_table=(page_table_t *)cur_dir->page_dir_entries[i/1024].addr;
		}else {
			page_table=(page_table_t *)k_alloc_pa(sizeof(page_table_t));
			install_page_table(cur_dir,i/1024,page_table,false,true);
		}
		install_page(page_table, i%1024, start, false, true);
		start+=0x1000;
		flush_tlb();
	}
}