/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __PAGING_H
#define __PAGING_H
#include "../common.h"
/*
 * layout of page directory entry
 * +---------+--------+----------+------+------+----------+------+-----------+---------+
 * |31  -  12| 11 - 9 | 8 and 7  |   6  |   5  | 4 and 3  |   2  |     1     |    0    |
 * +---------+--------+----------+------+------+----------+------+-----------+---------+
 * | Address | Unused | Reserved | Zero | Read | Reserved | User | Writable  | Present |
 * +---------+--------+----------+------+------+----------+------+-----------+---------+
 *
 *
 *
 * layout of page table entry
 * +------------------+--------+----------+-------+------+----------+------+-----------+---------+
 * |31       -      12| 11 - 9 | 8 and 7  |   6   |   5  | 4 and 3  |   2  |     1     |    0    |
 * +------------------+--------+----------+-------+------+----------+------+-----------+---------+
 * | physical address | Unused | Reserved | Dirty | Read | Reserved | User | Writable  | Present |
 * +------------------+--------+----------+-------+------+----------+------+-----------+---------+
 */

typedef struct packed_s pte{
    bool present   : 1;
    bool writable  : 1;
    bool userread  : 1;
    uint8_t resv1  : 2;
    bool read      : 1;
    bool dirty     : 1;
    uint8_t resv2  : 2;
    uint8_t unused : 3;
    uint32_t addr  : 20;
} page_table_e;

typedef struct pt {
    page_table_e page_entries[1024];
} page_table_t;

typedef struct packed_s pde{
    bool present   : 1;
    bool writable  : 1;
    bool userread  : 1;
    uint8_t resv1  : 2;
    bool read      : 1;
    bool zero      : 1;
    uint8_t resv2  : 2;
    uint8_t unused : 3;
    uint32_t addr  : 20;
} page_dir_e;

typedef struct pd {
    page_dir_e page_dir_entries[1024];
} page_dir_t;

typedef uint32_t PAGE;

void install_page(page_table_t *table,int index, void * physical_addr,bool user,bool writable);

void install_page_table(page_dir_t * dir,int index,page_table_t * table,bool user, bool writable);

void switch_to(page_dir_t * dir);

void flush_tlb();

void invl_page(int page);

void enable_paging();

void init_paging();

void ident_map(void *start,uint32_t size);

#endif