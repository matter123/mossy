/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "../common.h"
#include "key_trans.h"
#include "../lang/keycode/generic.h"

void *callback_func_key=0;


uint16_t modset;

void translate(bool press, uint8_t key);
void setmodKey(bool press, int modKey);
void initshift();

uint8_t shift[256];

#define KEY_CAPSLOCK 0
#define KEY_LSHIFT   1
#define KEY_LMETA    2
#define KEY_RALT     3
#define KEY_RMETA    4
#define KEY_LCTRL    5
#define KEY_RCTRL    6
#define KEY_RSHIFT   7
#define KEY_LALT     8

#define KEY_P_CAPSLOCK 0x15
#define KEY_P_LSHIFT   0x16
#define KEY_P_LMETA    0x17
#define KEY_P_LALT     0x18
#define KEY_P_RALT     0x19
#define KEY_P_RMETA    0x1A
#define KEY_P_RSHIFT   0x1D
#define KEY_P_LCTRL    0x1E
#define KEY_P_RCTRL    0x1F

void install_key_trans_callback(void (*callback)(uint8_t letter)) {
	callback_func_key=callback;
}

void init_key_trans() {
	install_callback(&translate);
	initshift();
}

void callbacker(uint8_t trans) {
	if(callback_func_key) {
		void (*callback)(uint8_t letter)=callback_func_key;
		callback(trans);
	}
}

void translate(bool press, uint8_t key) {
	uint8_t trans;
	switch(key) {
		case KEY_P_CAPSLOCK:
			setmodKey(press, KEY_CAPSLOCK);
			break;
		case KEY_P_LSHIFT:
			setmodKey(press, KEY_LSHIFT);
			break;
		case KEY_P_LMETA:
			setmodKey(press, KEY_LMETA);
			break;
		case KEY_P_LALT:
			setmodKey(press, KEY_LALT);
			break;
		case KEY_P_RALT:
			setmodKey(press, KEY_RALT);
			break;
		case KEY_P_RMETA:
			setmodKey(press, KEY_RMETA);
			break;
		case KEY_P_RSHIFT:
			setmodKey(press, KEY_RSHIFT);
			break;
		case KEY_P_LCTRL:
			setmodKey(press, KEY_LCTRL);
			break;
		case KEY_P_RCTRL:
			setmodKey(press, KEY_RCTRL);
			break;
		default:
			if(key>='A'&&key<='Z') {
				trans=key+('a'-'A');
				if(isCapslockActive()&&!isShiftPressed())trans=key;
				if(!isCapslockActive()&&isShiftPressed())trans=key;
				
			}else {
				if(isShiftPressed()) {
					trans=shift[key];
					if(!trans)trans=key;
				}else trans=key;
			}
			if(press)
				callbacker(trans);
			break;
	}
}

bool ismodKeyPressed(int modKey) {
	return modset&(1<<modKey);
}
bool capsactive;
void setmodKey(bool press, int modKey) {
	if(modKey==KEY_CAPSLOCK&&press&&(!ismodKeyPressed(KEY_CAPSLOCK)))
			capsactive=!capsactive;
	if(press)
		modset|=(1<<modKey);
	else
		modset&=~(1<<modKey);
}

bool isCapslockActive() {
	//debug("capscheck: %b\n",capsactive);
	return capsactive;
}

bool isShiftPressed() {
	bool shiftc=ismodKeyPressed(KEY_LSHIFT)||ismodKeyPressed(KEY_RSHIFT);
	//debug("shiftcheck: %b\n",shiftc);
	return shiftc;
}

bool isCtrlPressed() {
	return ismodKeyPressed(KEY_LCTRL)||ismodKeyPressed(KEY_RCTRL);
}

bool isAltPressed() {
	return ismodKeyPressed(KEY_LALT)||ismodKeyPressed(KEY_RALT);
}

void initshift() {
	//todo put in a table
	shift['`']='~';
	shift['1']='!';
	shift['2']='@';
	shift['3']='#';
	shift['4']='$';
	shift['5']='%';
	shift['6']='^';
	shift['7']='&';
	shift['8']='*';
	shift['9']='(';
	shift['0']=')';
	shift['-']='_';
	shift['=']='+';
	
	shift['[']='{';
	shift[']']='}';
	shift['\\']='|';
	
	shift[';']=':';
	shift['\'']='"';
	
	shift[',']='<';
	shift['.']='>';
	shift['/']='?';
}