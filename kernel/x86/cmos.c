/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "cmos.h"
#include <io.h>

int NMI=0;

void enableNMI() {
	NMI=0;
	outb(0x70,NMI|0x00);
	inb(0x71);
}

void disableNMI() {
	NMI=1<<7;
	outb(0x70,NMI|0x00);
	inb(0x71);
}

bool isNMIenabled() {
	return NMI==0;
}

uint8_t read(uint8_t reg) {
	outb(0x70,NMI|reg);
	return inb(0x71);
}
