/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "common.h"
#include <io.h>

#define RESET 0xFE
 
#define bit(n) (1<<(n))
#define check_flag(flags, n) ((flags) & bit(n))

void reboot(){
	cli();
	uint8_t temp;
	do{
		temp = inb(0x64);
		if (check_flag(temp, 0) != 0)inb(0x60);
	} while (check_flag(temp, 1) != 0);
	loop:
	outb(0x64, RESET);
	hlt();
	goto loop;
}