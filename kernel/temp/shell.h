/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __SHELL_H
#define __SHELL_H
#include <string.h>

#define MAX_CMD_COUNT  32
#define MAX_CMD_LENGTH 16

void init_shell();

void pass_control();

void install_command(string name,void(*cmd)(string));

#endif

