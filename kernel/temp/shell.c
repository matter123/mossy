/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include <stdio.h>
#include <console.h>
#include <string.h>
#include <io.h>
#include "shell.h"
#include "../speaker.h"
#include "../x86/pit.h"
#include "../x86/VGA.h"

string cmds[MAX_CMD_COUNT];
void *cmd_ptr[MAX_CMD_COUNT];
char bufa[80];
int next_bufa=0;
static string buf=(string)bufa;
void handle_cmd();
bool equals(string bufs,string cmd);
void twinkle(string args);
void color(string args);
void calc(string args);

uint8_t cmd_count=0;

void pass_control() {
	puts("READY\n");
	beep(10);
	//TODO refactor into a new method
	while(1) {
		putchar('>');
		//this loop provides 
		while(bufa[next_bufa-1]!='\n'&&!(next_bufa>=80)) {
			bufa[next_bufa]=getchar();
			if(bufa[next_bufa]=='\b') {
				if(next_bufa>0) {
					next_bufa--;
					unputc();
				}else {
					beep_freq(492,1);
				}
			}else
				putchar(bufa[next_bufa++]);
		}
		bufa[next_bufa-1]=0;
		//at this point its a null terminated string with no newline
		handle_cmd();
		memset((uint8_t *)buf,0,80);
		next_bufa=0;
	}
}

void help(string args) {
	printf("there are %d commands:\n",cmd_count);
	for(int i=0;i<cmd_count;i++) {
		puts("  ");
		puts(cmds[i]);
		puts("\n");
	}
}
void echo(string args) {
	puts(args);
	putchar('\n');
}

void clear(string args) {
	clearScreen();
}

void time(string args) {
	printf("it has been %d seconds since boot\n",get_seconds());
}

void reset(string args) {
	reboot();
}

void init_shell() {
	clearScreenColor(COLOR_BLACK);
	install_command("help",&help);
	install_command("echo",&echo);
	install_command("color",&color);
	install_command("twinkle",&twinkle);
	install_command("clear",&clear);
	install_command("utime",&time);
	install_command("reset", &reset);
	install_command("calc", &calc);
	//disable_cursor();
}

void install_command(string name,void(*cmd)(string args)) {
	if(cmd_count==MAX_CMD_COUNT)return;
	if(strlen(name)>=16)name[15]='\0';
	cmds[cmd_count]=name;
	cmd_ptr[cmd_count]=cmd;
	cmd_count++;
}

bool equals(string bufs,string cmd) {
	return strcmp(bufs,cmd)==0;
}

void handle_cmd() {
	/* this part finds the first space in the command,
	 * replaces it with \0 and sets args to the space plus 1
	 */
	printf("%s\n",buf);
	string args;
	int c=0;
	while(bufa[c]!=' '&&bufa[c]!='\0')c++;
	if(bufa[c]==' ')bufa[c]='\0';
	args=buf + (++c);
	
	//finds the command and executes it
	int index=-1;
	for(int i=0;i<cmd_count;i++) {
		if(equals(buf,cmds[i])) {
			index=i;
			break;
		}
	}
	if(index!=-1) {
		void (*cmd)(string)=cmd_ptr[index];
		cmd(args);
	}else {
		printf("unknown command '%s'. type help for a list of commands\n",buf);
		beep_freq(C5+C4,50);
	}
}

void twinkle(string args) {
	note(C4,QUARTER);
	note(C4,QUARTER);
	note(G4,QUARTER);
	note(G4,QUARTER);
	
	note(A4,QUARTER);
	note(A4,QUARTER);
	note(G4,HALF);
	
	note(F4,QUARTER);
	note(F4,QUARTER);
	note(E4,QUARTER);
	note(E4,QUARTER);
	
	note(D4,QUARTER);
	note(D4,QUARTER);
	note(C4,HALF);
	
	
	note(G4,QUARTER);
	note(G4,QUARTER);
	note(F4,QUARTER);
	note(F4,QUARTER);
	
	note(E4,QUARTER);
	note(E4,QUARTER);
	note(D4,HALF);
	
	note(G4,QUARTER);
	note(G4,QUARTER);
	note(F4,QUARTER);
	note(F4,QUARTER);
	
	note(E4,QUARTER);
	note(E4,QUARTER);
	note(D4,HALF);
	
	
	note(C4,QUARTER);
	note(C4,QUARTER);
	note(G4,QUARTER);
	note(G4,QUARTER);
	
	note(A4,QUARTER);
	note(A4,QUARTER);
	note(G4,HALF);
	
	note(F4,QUARTER);
	note(F4,QUARTER);
	note(E4,QUARTER);
	note(E4,QUARTER);
	
	note(D4,QUARTER);
	note(D4,QUARTER);
	note(C4,HALF);
}
uint8_t charHexToInt(char h) {
	switch(h) {
		case '0':
			return 0x0;
		case '1':
			return 0x1;
		case '2':
			return 0x2;
		case '3':
			return 0x3;
		case '4':
			return 0x4;
		case '5':
			return 0x5;
		case '6':
			return 0x6;
		case '7':
			return 0x7;
		case '8':
			return 0x8;
		case '9':
			return 0x9;
		case 'A':
			return 0xA;
		case 'B':
			return 0xB;
		case 'C':
			return 0xC;
		case 'D':
			return 0xD;
		case 'E':
			return 0xE;
		case 'F':
			return 0xF;
	}
	return 0;
}
void color(string args) {
	if(equals(args,"test")) {
		int back=getBGColor();
		int front=getFGColor();
		puts("color BACK FRONT\n");
		setWriteColor(COLOR_BLUE,back);
		puts("blue,1 ");
		setWriteColor(COLOR_GREEN,back);
		puts("green,2 ");
		setWriteColor(COLOR_CYAN,back);
		puts("cyan,3 ");
		setWriteColor(COLOR_RED,back);
		puts("red,4 ");
		setWriteColor(COLOR_MAGENTA,back);
		puts("magenta,5 ");
		setWriteColor(COLOR_BROWN,back);
		puts("brown,6 ");
		setWriteColor(COLOR_LGRAY,back);
		puts("light gray,7 ");
		
		setWriteColor(COLOR_GRAY,back);
		puts("gray,8 ");
		setWriteColor(COLOR_LBLUE,back);
		puts("light blue,9\n");
		setWriteColor(COLOR_LGREEN,back);
		puts("light green,A ");
		setWriteColor(COLOR_LCYAN,back);
		puts("light cyan,B ");
		setWriteColor(COLOR_LRED,back);
		puts("light red,C ");
		setWriteColor(COLOR_LMAGENTA,back);
		puts("light magenta,D ");
		setWriteColor(COLOR_YELLOW,back);
		puts("yellow,E ");
		setWriteColor(COLOR_WHITE,back);
		puts("white,F\n");
		setWriteColor(front,back);
		return;
	}
	uint8_t back  = charHexToInt(args[0]);
	uint8_t front = charHexToInt(args[2]);
	setWriteColor(front,back);
}
