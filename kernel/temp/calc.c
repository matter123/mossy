/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "shell.h"
#include <string.h>
#include <stack.h>
#include <stdlib.h>

STACK s=NULL;

void handle(string op);

void calc(string args) {
	if(s==NULL)s=createStack(100);
	int len=strlen(args);
	for(int i=0;i<len;i++) {
		if(args[i]==' ')args[i]='\0';
	}
	for(int i=0;i<len;i++) {
		//call handle on every segment
		if(i==0||(args[i]=='\0'&&i+1!=len))
			handle((args+(i?i+1:0)));
	}
	printf("%d\n",peek(s));
}

void handle(string arg) {
	if(strlen(arg)==1) {
		switch(*arg) {
			case '+':
				push(s,pop(s)+pop(s));
				return;
			case '*':
				push(s,pop(s)*pop(s));
				return;
			case '-':
				roll(s);
				push(s,pop(s)-pop(s));
				return;
			case '/':
				roll(s);
				push(s,pop(s)/pop(s));
				return;
			case 'f':
				clearStack(s);
				return;
			case 'r':
				push(s,peek(s));
				return;
			case '!':
				push(s,factorial(pop(s)));
				return;
			case '^':
				push(s,pow(pop(s),pop(s)));
				return;
		}
	}
	//push a non op
	push(s,atoi(arg));
}
