/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "x86/idt.h"

extern void * syscall0; //exit			int $0x30
extern void * syscall1; //port IO		int $0x31
extern void * syscall2; //file IO		int $0x32
extern void * syscall3; //user IO		int $0x33
extern void * syscall4; //memory edit	int $0x34

typedef struct syscall_struct {
    uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; //general purpose registers, eax is function number
    uint32_t syscall, zero; //system call, and an always zero
    uint32_t eip, cs, eflags, useresp, ss; //CPU state
} syscall_t;


int syscall_handler(syscall_t sys) {
	debug("system call executed %d action %d",sys.syscall,sys.eax);
	return 5;
}

void init_syscall() {
	idt_set_gate(0x60, (uint32_t)&syscall0, 0x08, 0x8E);
	idt_set_gate(0x61, (uint32_t)&syscall1, 0x08, 0x8E);
	idt_set_gate(0x62, (uint32_t)&syscall2, 0x08, 0x8E);
	idt_set_gate(0x63, (uint32_t)&syscall3, 0x08, 0x8E);
	idt_set_gate(0x64, (uint32_t)&syscall4, 0x08, 0x8E);
}

void install_syscall_handler(int syscall,void (*handler)(registers_t *reg)) {

}
