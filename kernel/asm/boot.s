;  Copyright 2013 Matthew Fosdick

;  Licensed under the Apache License, Version 2.0 (the "License");
;  you may not use this file except in compliance with the License.
;  You may obtain a copy of the License at

;    http://www.apache.org/licenses/LICENSE-2.0

;  Unless required by applicable law or agreed to in writing, software
;  distributed under the License is distributed on an "AS IS" BASIS,
;  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;  See the License for the specific language governing permissions and
;  limitations under the License.


[BITS 32]
[EXTERN panic]
[EXTERN main]
[EXTERN asm_panic]

;defines
MAGIC         equ 0x1BADB002
FLAGS         equ (1<<0)+(1<<1)+(1<<2) ; page align, memory info,video
CHECKSUM      equ -(MAGIC+FLAGS)
KERNEL_VIRTUAL_BASE equ 0xC0000000 ;3GB
KERNEL_DIR_NUMBER equ (KERNEL_VIRTUAL_BASE >> 22);make a page dir number

;multiboot header
[SECTION .multiboot]
ALIGN 4
[GLOBAL mboot]
mboot:
    magic      dd  MAGIC
    flags      dd  FLAGS
    check      dd  CHECKSUM
	times 20   db  0
	mode       dd  0
	width      dd  640
	height     dd  480
	depth      dd  32

[SECTION .bootstrap_stack]
stack_bottom:
times 16384 db 0
stack_top:

[SECTION .pagedir]


[SECTION .text]
[GLOBAL start]
start:
    mov esp, stack_top ; use stack
    cli
    cmp eax, 0x2BADB002
    jne no_mboot
    push ebx
    call main
    call asm_panic
    jmp $

no_mboot:
    mov eax, 2
    push eax
    jmp $