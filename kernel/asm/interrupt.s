;  Copyright 2013 Matthew Fosdick

;  Licensed under the Apache License, Version 2.0 (the "License");
;  you may not use this file except in compliance with the License.
;  You may obtain a copy of the License at

;    http://www.apache.org/licenses/LICENSE-2.0

;  Unless required by applicable law or agreed to in writing, software
;  distributed under the License is distributed on an "AS IS" BASIS,
;  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;  See the License for the specific language governing permissions and
;  limitations under the License.

[GLOBAL idt_load]
[EXTERN idtp]
idt_load:
    lidt [idtp]
    ret

;idt installs

 %macro ISR_NOERRCODE 1
  [GLOBAL isr%1]
  isr%1:
    push byte 0
    push byte %1
    jmp isr_common_stub
%endmacro

%macro ISR_ERRCODE 1
  [GLOBAL isr%1]
  isr%1:
    push byte %1
    jmp isr_common_stub
%endmacro

%macro IRQ 1
  [GLOBAL irq%1]
  irq%1:
    push byte 0
    push byte %1
    jmp irq_common_stub
%endmacro

ISR_NOERRCODE 0
ISR_NOERRCODE 1
ISR_NOERRCODE 2
ISR_NOERRCODE 3
ISR_NOERRCODE 4
ISR_NOERRCODE 5
ISR_NOERRCODE 6
ISR_NOERRCODE 7
ISR_ERRCODE   8
ISR_NOERRCODE 9
ISR_ERRCODE   10
ISR_ERRCODE   11
ISR_ERRCODE   12
ISR_ERRCODE   13
ISR_ERRCODE   14
ISR_NOERRCODE 15
ISR_NOERRCODE 16
ISR_NOERRCODE 17
ISR_NOERRCODE 18
ISR_NOERRCODE 19
ISR_NOERRCODE 20
ISR_NOERRCODE 21
ISR_NOERRCODE 22
ISR_NOERRCODE 23
ISR_NOERRCODE 24
ISR_NOERRCODE 25
ISR_NOERRCODE 26
ISR_NOERRCODE 27
ISR_NOERRCODE 28
ISR_NOERRCODE 29
ISR_NOERRCODE 30
ISR_NOERRCODE 31

IRQ           0
IRQ           1
IRQ           2
IRQ           3
IRQ           4
IRQ           5
IRQ           6
IRQ           7
IRQ           8
IRQ           9
IRQ           10
IRQ           11
IRQ           12
IRQ           13
IRQ           14
IRQ           15

[EXTERN isr_handler]
isr_common_stub:
  pushad   ;push EAX EBX ECX EDX etc
  call isr_handler
  popad
  add esp,8
  iret

[EXTERN irq_handler]
irq_common_stub:
  pushad   ;push EAX EBX ECX EDX etc
  call irq_handler
  popad
  add esp,8
  iret