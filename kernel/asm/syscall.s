;  Copyright 2013 Matthew Fosdick

;  Licensed under the Apache License, Version 2.0 (the "License");
;  you may not use this file except in compliance with the License.
;  You may obtain a copy of the License at

;    http://www.apache.org/licenses/LICENSE-2.0

;  Unless required by applicable law or agreed to in writing, software
;  distributed under the License is distributed on an "AS IS" BASIS,
;  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;  See the License for the specific language governing permissions and
;  limitations under the License.

[EXTERN syscall_handler]
syscall_common_stub:
	pushad   ;push EAX EBX ECX EDX etc
	call syscall_handler
	push eax
	popad
	add esp,8
	iret
  
%macro SYSCALL 1
[GLOBAL syscall%1]
syscall%1:
	push 0
    push %1
    jmp syscall_common_stub
%endmacro

SYSCALL       0 ;exit
SYSCALL       1 ;port IO
SYSCALL       2 ;file IO
SYSCALL       3 ;user IO
SYSCALL       4 ;memory edit