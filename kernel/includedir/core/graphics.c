/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <core/graphics.h>
#include "../../x86/VGA.h"

struct packed_s vmode {
	uint16_t width;
	uint16_t height;
	uint8_t depth;
	uint8_t Bpp;
	uint16_t bpsl;
	uint32_t physAddr;
};

struct vmode current;
uint8_t* mon;

void initGraphics() {
	VBE_mode_t *curmode=current_mode();
	current.width=curmode->XRes;
	current.height=curmode->YRes;
	current.depth=curmode->bpp;
	current.bpsl=curmode->BytesPerScanline;
	current.physAddr=curmode->physAddr;
	current.Bpp=current.depth/8;
	mon=(uint8_t *)current.physAddr;
}

void ClearScreenSolid(Color c) {
	int end=current.bpsl*current.height;
	int i=0;
	while(i<end) {
		if(i%current.bpsl>current.Bpp*current.width)i=current.bpsl*((i/current.bpsl)+1);
		//internally BGR
		mon[i++]=c.Blue;
		mon[i++]=c.Green;
		mon[i++]=c.Red;
	}
}

void DrawBoxSolid(Color c, POINT p,int width,int height) {
	for(int y=p.Y;y<p.Y+height;y++) {
		int offset=current.bpsl*y+p.X*current.Bpp;
		for(int x=0;x<width;x++) {
			mon[offset++]=c.Blue;
			mon[offset++]=c.Green;
			mon[offset++]=c.Red;
		}
	}
}

void swap(int *a,int *b) {
	int temp=*a;
	*a=*b;
	*b=temp;
}

void inline plot(int x,int y,Color c) {
	int startpos=y*current.bpsl+x*current.Bpp;
	mon[startpos++]=c.Red;
	mon[startpos++]=c.Blue;
	mon[startpos++]=c.Green;
}
//Bresenham's line drawing algorithm
void DrawLine(Color c, POINT start,POINT end) {
	int x0=start.X;
	int y0=start.Y;
	int x1=end.X;
	int y1=end.Y;
	bool steep=abs(y1-y0)>abs(x1-x0);
	if(steep) {
		swap(&x0,&x1);
		swap(&y0,&y1);
	}
	if(x0>x1) {
		swap(&x0,&x1);
		swap(&y0,&y1);
	}
	int deltax=x1-x0;
	int deltay=abs(y1-y0);
	int error=deltax/2;
	int ystep=(y0<y1?1:-1);
	int y=y0;
	for(int x=x0;x<x1;x++) {
		if(steep)plot(y,x,c);
		else plot(x,y,c);
		error-=deltay;
		if(error<0) {
			y+=ystep;
			error+=deltax;
		}
	}
}

Color ColorFromRGB(uint8_t Red,uint8_t Green, uint8_t Blue) {
	Color c;
	c.Alpha=0xFF;
	c.Red=Red;
	c.Green=Green;
	c.Blue=Blue;
	return c;
}