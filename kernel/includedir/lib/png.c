/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <lib/png.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
 
bool isValid(STREAM s) {
	uint32_t seek=seekpos(s);
	uint32_t magic1=readU32(s);
	uint32_t magic2=readU32(s);
	printf("magic1=%x magic2=%x\n",magic1,magic2);
	return true;
}

png_t * parsePNG(STREAM s) {
	return NULL;
}

