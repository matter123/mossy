/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef __PNG_H
#define __PNG_H
#include <stdlib.h>
#include <stream.h>
#include <stdbool.h>

//any function taking a STREAM, must have the stream seek, to the start of the png file
//normally seek(s,0);

typedef struct png {
	int width;
	int height;
	int mode;
	int depth;
	STREAM s;
}png_t;

bool isValid(STREAM s);

png_t * parsePNG(STREAM s);
#endif

