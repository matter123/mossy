/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include "common.h"
#include "keyboard.h"
#include "debug.h"
#include "ps2.h"
#include "lang/keycode/generic.h"
#include "x86/keycodes.h" //keycode list is hiding
#include <kmath.h>

void kbd_callback(uint8_t scancode);
void (*key_interp_func)(uint16_t key);

void key_change(bool press, uint8_t key);

int no_optimize init_keyboard(int port) {
	init_extended_keys();
	ps2_install_callback(port, &kbd_callback);
	int i=1;
	uint8_t byte=0;


	//set scan code 2
	sendbyte(port, 0xF0);
	byte=readbyte();
	if (!ISACK(byte))return i;
	sendbyte(port, 0x2);
	byte=readbyte();
	if (!ISACK(byte))return i;
	i++;

	//disable all lights
	sendbyte(port, 0xED);
	byte=readbyte();
	if (!ISACK(byte))return i;
	sendbyte(port, 0x0);
	byte=readbyte();
	if (!ISACK(byte))return i;
	i++;

	//enable scanning last problem area
	sendbyte(port, 0xF4);
	byte=readbyte();
	if (!ISACK(byte))return i;
	
	install_callback(&key_change);
	return 0;
}

key_t cur_key;

void newkey(key_t *key);
void use(key_t key);
int sskip=0;
void setskip(int skip);
bool skip();

void kbd_callback(uint8_t scancode) {
	if (skip())return;
	if (oreq(scancode, 0xE0, 0xE1)) {
		cur_key.extended=true;
		if (scancode==0xE1) {
			cur_key.pausebut=true;
			cur_key.released=true;
			cur_key.letter=0x7F; //arbitrary scancode based off scroll lock
			setskip(7);
			use(cur_key);
			newkey(&cur_key);
		}
		return;
	}
	if (scancode==0xF0) {
		cur_key.released=true;
		return;
	}
	cur_key.letter=scancode;
	use(cur_key);
	newkey(&cur_key);
}

void newkey(key_t *key) {
	memset((uint8_t *)key, 0, sizeof (key_t));
}

void use(key_t key) {
	uint16_t newscan=key.letter+(key.extended?0x88:0x00);
	uint16_t keycode=keycodes[newscan];
	if (key.released)keycode|=(1<<15);
	if (key_interp_func)key_interp_func(keycode);
}

void setskip(int i) {
	sskip=i;
}

bool skip() {
	if (sskip==0)return false;
	sskip--;
	return true;
}

void keyboard_install_keycode_interp(void (*key_interp)(uint16_t key)) {
	key_interp_func=key_interp;
}

uint8_t dmap[256/8];

bool isKeyPressed(uint8_t key) {
	return dmap[key/8]&(1<<(key%8));
}

void key_change(bool press, uint8_t key) {
	if(press)
		dmap[key/8]=dmap[key/8]|(1<<(key%8));
	else
		dmap[key/8]=dmap[key/8]&(~(1<<(key%8)));
}
