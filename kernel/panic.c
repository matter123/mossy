/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include "common.h"
#include "panic.h"

#include <string.h>
#include <console.h>
#include <io.h>

void no_optimize panic(int code,...) {
	POINT write;
	va_list arg;
	va_start(arg,code);
	cli();
	bool printcode=false;
	char * message="Error Code is ";
	switch(code) {
		case 0:
			message="Error Code is Unknown";
			break;
		case 1:
			message="Assembly Error";
			break;
		case PANIC_PAGE_FAULT:
			message="Page fault, Address is ";
			printcode=true;
			asm volatile("mov %%cr2, %0":"=r"(code));
			code|=(va_arg(arg,unsigned int)&0x3);
			break;
		case PANIC_OUT_MEM:
			message="Out of Memory";
			break;
			
		case PANIC_NO_RAMDISK:
			message="No Ram Disk Present, or Corrupted Ram Disk";
			break;
		default:
			printcode=true;
			break;
	}

	//setup monitor
	clearScreenColor(COLOR_RED);
	setWriteColor(COLOR_WHITE,COLOR_RED);
	string startmsg="KERNAL PANIC";

	//center startmsg on screen
	int left=40-(strlen(startmsg)/2);
	write.X=left;
	write.Y=5;
	setWritePos(write);

	puts(startmsg);

	//center message and, if applicable, the error code
	left=40-(strlen(message)/2);
	if (printcode) {
		int err=code;
		int numleft=1;
		while (err>=16) {
			err=err/16;
			numleft++;
		}
		left-=(numleft/2)+2;
	}
	write.X=left;
	write.Y=7;
	setWritePos(write);

	puts(message);
	char hex[33];
	if (printcode) {
		puts(itoa(code,hex,16));
	}

	message="Restarting ...";

	//once again center
	left=40-(strlen(message)/2);
	write.X=left;
	write.Y=9;
	setWritePos(write);

	puts(message);

	//disable_cursor();
	//delay based off cpu speed and presence of an error code
	for(int i=0;i<(printcode?4:1);i++)for(int j=0;j<0x1000000-(0x10*(CPU-386));j++)nop();
	reboot();
}

void isr_panic(int isr, int error) {
	if (isr==0)isr=~isr;
	panic(((isr&0xFFFF)<<16)|(error&0xFFFF));
}

void uk_panic() {
	panic(0);
}

void asm_panic() {
	panic(1);
}

