/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */


#include "mem.h"
#include <string.h>
#include "debug.h"
#include <stdlib.h>

extern void *k_end; //end of kernel
extern void *k_start; //start of kernel

static mmap_t *map=(void *)&k_end;

void parseTables();

typedef struct int_mmap {
	uint32_t size;
	uint32_t base_addr_low;
	uint32_t base_addr_high;
	uint32_t length_low;
	uint32_t length_high;
	uint32_t type;
} int_mmap_t;

static int region_c;
static uint32_t maxram;

uint32_t kernel_end_addr() {
	return (uint32_t)(&k_end+region_c*sizeof (mmap_t));
}

uint32_t kernel_size() {
	return (uint32_t)(kernel_end_addr()-(uint32_t)(&k_start));
}

int mem_region_count() {
	return region_c;
}

mmap_t *mem_region(int region) {
	return &map[region];
}
uint32_t kalloc_ptr;

void init_mem(uint32_t mmapAddr, int length,uint32_t startAddr,uint32_t upmem) {
	int_mmap_t *mmap=(int_mmap_t *)mmapAddr;
	region_c=length/sizeof (int_mmap_t);
	memset(k_end, 0, sizeof (mmap_t)*region_c); //clear enough room
	for (int i=0; i<region_c; i++) {
		int_mmap_t *cur=&mmap[i];
		mmap_t *set=&map[i];
		set->addr=(((uint64_t)cur->base_addr_high<<32)|cur->base_addr_low); //store the two 32 bits as one 64
		set->length=(((uint64_t)cur->length_high<<32)|cur->length_low); //same as above but for length
		set->type=cur->type;
	}
	kalloc_ptr=startAddr;
	assert(startAddr>mmapAddr+((unsigned)length));
	maxram=upmem+0x400;
	parseTables();
}
uint8_t *nresv_mem;

void parseTables() {
	debug("%d\n",maxram);
	//first pass calculate room
	uint32_t room=(maxram/8)+(maxram%8?1:0);
	debug("%d\n",room);
	nresv_mem=(uint8_t *)malloc(room);
	memset(nresv_mem,0,room);

	for(int i=0;i<region_c;i++) {
		mmap_t region=map[i];
		if(region.addr>=maxram*1024)continue;
		assert(!(region.addr%0x400||region.length%0x400));
		if(region.type==1) {
			uint32_t start=region.addr/1024;
			uint32_t end=(region.addr+region.length)/1024;
			debug("%x ",region.addr);
			debug("%x ",region.length);
			debug("%d\n",end-start);
			for(uint32_t j=start;j<end;j++) {
				//1kb sections
				nresv_mem[j/8]|=(1<<(j%8));
			}
		}
	}
}

int memstrcmp(char *str1, char *str2) {
	while (*str2)
		if (*str1++!=*str2++)return 1;
	return 0;
}

uint32_t memsearch_j(uint32_t start, uint32_t end, uint32_t jump, string str) {
	char *mem;
	for (mem=(char *)start; mem<(char *)end; mem+=jump?:1) {
		if (memstrcmp(mem, str)==0) {
			return (uint32_t)mem;
		}
	}
	//not found
	return 0xBAD;
}

uint32_t memsearch(uint32_t start, uint32_t end, string str) {
	return memsearch_j(start, end, 1, str);
}


//TODO test on real machine for benchmarking

void memcpy(uint8_t *dest, const uint8_t *src, uint32_t len) {
	while(len-=4>3) { *dest++ = *src++; *dest++ = *src++; *dest++ = *src++; *dest++ = *src++;}
	while(len-->0) *dest++ = *src++;
}

void memset(uint8_t *dest, uint8_t val, uint32_t len) {
	while(len--) dest[len] =val;
}

void memset16(uint16_t *dest, uint16_t val, uint32_t len) {
	while(len--) dest[len] =val;
}

void memset32(uint32_t *dest, uint32_t val, uint32_t len) {
	while(len--) dest[len]=val;
}

uint32_t k_alloci(bool pa,uint32_t size) {
    if (pa&&(kalloc_ptr & 0x0FFF)) {
		kalloc_ptr &= 0xFFFFF000;
		kalloc_ptr += 0x1000;
	}
	uint32_t temp=kalloc_ptr;
	kalloc_ptr+=size;
	return temp;
}

void * k_alloc(uint32_t size) {
    return (void *)k_alloci(false,size);
}

void * k_alloc_pa(uint32_t size) {
    return (void *)k_alloci(true,size);
}

uint32_t k_alloc_addr() {
	return kalloc_ptr;
}

uint32_t ramleft() {
	return 0;
}

uint32_t ramsize() {
	return maxram;
}
