/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "vfs.h"
#include "../mem.h"
#include "dev.h"
#include "node.h"
#include <string.h>

node_t *root;

uint32_t nextFileID=1;

void init_vfs() {
	debug("loading vfs\n");
	root=(node_t *)malloc(sizeof(node_t));
	strcpy(root->name,"ROOT_DIR     DIR");
	for(int i=0;i<3;i++)root->resv[i]=0;
	root->attrib=VFS_NODE_SYSTEM|VFS_NODE_MOUNTPOINT|VFS_NODE_DIR;
	root->ID=nextFileID++;
	root->parent=0;
	root->child_count=0;
	root->sib_left=0;
	root->sib_right=0;
	root->device=0;
	
	debug("creating children\n");
	createNode("DEVICE       DIR",root,VFS_NODE_SYSTEM|VFS_NODE_DIR);
	createNode("BIN          DIR",root,VFS_NODE_SYSTEM|VFS_NODE_DIR);
	createNode("USR          DIR",root,VFS_NODE_DIR);
	createNode("TEMP         DIR",root,VFS_NODE_SYSTEM|VFS_NODE_DIR);
}

bool checkflag(node_t *node, int flag) {
	if(flag<0x4) { //the ME attributes
		uint16_t attrib=node->attrib;
		attrib&=0x3;
		return attrib==flag;
	}else {
		uint16_t attrib=node->attrib;
		attrib&=flag;
		return attrib;
	}
}

int readFile(node_t *file,uint32_t offset,uint32_t size,void *buffer) {
	if(file==0) {
		return 0;
	}
	if(file->device==0){
		return 0;
	}
	if(isSymbolicLink(file))return readFile(file->first_child,offset,size,buffer);
	if(!isFile(file))return 0;
	struct dev *device=(dev_t *)file->device;
	int (*read_func)(void *node,uint32_t size,uint32_t offset,void *buffer)=device->read_func;
	if(read_func==0) {
		return 0;
	}
	return read_func((void *)file,size,offset,buffer);
}

int writeFile(node_t *file,uint32_t offset,uint32_t size,void *buffer) {
	if(file==0) {
		return 0;
	}
	if(file->device==0){
		return 0;
	}
	if(isSymbolicLink(file))return readFile(file->first_child,offset,size,buffer);
	if(!isFile(file))return 0;
	struct dev *device=(dev_t *)file->device;
	int (*write_func)(void *node,uint32_t size,uint32_t offset,void *buffer)=device->write_func;
	if(write_func==0) {
		return 0;
	}
	return write_func((void *)file,size,offset,buffer);
}

bool canRead(node_t *file) {
	return checkflag(file,VFS_NODE_READ);
}

bool canWrite(node_t *file) {
	return checkflag(file,VFS_NODE_WRITE);
}

bool canExecute(node_t *file) {
	return checkflag(file,VFS_NODE_EXEC);
}

bool isSystemFile(node_t *file) {
	return checkflag(file,VFS_NODE_SYSTEM);
}

bool isCompressed(node_t *file) {
	return checkflag(file,VFS_NODE_COMPRESSED);
}

bool isEncrypted(node_t *file) {
	return checkflag(file,VFS_NODE_ENCRYPTED);
}

bool isFile(node_t *file) {
	return checkflag(file,VFS_NODE_FILE);
}

bool isSymbolicLink(node_t *file) {
	return checkflag(file,VFS_NODE_LINK);
}

bool isDirectory(node_t *file) {
	return checkflag(file,VFS_NODE_DIR);
}

bool isMountpoint(node_t *file) {
	return checkflag(file,VFS_NODE_MOUNTPOINT);
}

int getChildrenCount(node_t *dir) {
	if(!isDirectory(dir))return 0;//allow abuse of child count
	return dir->child_count;
}

node_t *getChild(node_t *node,int childnum) {
	if(childnum<0||childnum>=node->child_count||(!isDirectory(node)))return NULL;
	node_t *child=node->first_child;
	for(int i=0;i<childnum;i++) {
		child=child->sib_right;
	}
	return child;
}

//name is the full 16 char name ie "ROOT_DIR     DIR" and not "ROOT_DIR.DIR"
node_t *findChild(node_t *parent,c_string name) {
	if(!isDirectory(parent))return (node_t *)0;
	node_t *child=parent->first_child;
	for(int i=0;i<parent->child_count;i++) {
		if(!strcmp(child->name,name))return child;
		child=child->sib_right;
	}
	return (node_t *)0;
}

node_t *getRoot() {
	return root;
}

void addChild(node_t *parent, node_t * child) {
	if(!isDirectory(parent))return;
	child->parent=parent;
	if(parent->child_count==0) {
		
		parent->first_child=child;
		child->sib_left=0;
		child->sib_right=0;
	}else {
		node_t *tchild=parent->first_child;
		while(tchild->sib_right)tchild=tchild->sib_right;
		tchild->sib_right=child;
		child->sib_left=tchild;
	}
	parent->child_count++;
}

string getSystemName(c_string common) {
	string res=(string)malloc(17);
	memset((uint8_t *)res,0,17);
	
	int afterdot=0;
	for(int i=0;i<13;i++) {
		if(common[i]=='.') {
			afterdot=i+1;
		}
		if(afterdot)res[i]=' ';
		else res[i]=common[i];
	}
	for(int i=13;i<17;i++) {
		res[i]=common[afterdot+(i-13)];
	}
	return res;
}

node_t *createNode(c_string name,node_t *parent,uint32_t flags) {
	node_t *node=(node_t *)malloc(sizeof(node_t));
	strcpy(node->name,name);
	for(int i=0;i<3;i++)node->resv[i]=0;
	node->attrib=flags;
	node->ID=nextFileID++;
	node->device=0;
	node->sib_left=0;
	node->sib_right=0;
	node->child_count=0;
	addChild(parent,node);
	return node;
}
