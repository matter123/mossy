/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __VFS_H
#define	__VFS_H
#include "node.h"
#include "../common.h"
#include "dev.h"
#include <stdlib.h>
#include <string.h>


void init_vfs();

int readFile(node_t *file,uint32_t offset,uint32_t size,void *buffer);

int writeFile(node_t *file,uint32_t offset,uint32_t size,void *buffer);

bool canRead(node_t *file);

bool canWrite(node_t *file);

bool canExecute(node_t *file);

bool isSystemFile(node_t *file);

bool isCompressed(node_t *file);

bool isEncrypted(node_t *file);

bool isFile(node_t *file);

bool isSymbolicLink(node_t *file);

bool isDirectory(node_t *file);

bool isMountpoint(node_t *file);

int getChildrenCount(node_t *dir);

node_t *findChild(node_t *parent,c_string name);

node_t *getChild(node_t *node,int child);

node_t *getRoot();

void addChild(node_t *parent, node_t * child);

string getSystemName(c_string common);

node_t *createNode(c_string name,node_t *parent,uint32_t flags);

node_t *resolvePath(c_string path, ErrorCode node);

#endif

