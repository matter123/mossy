/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	 http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __NODE_H
#define	__NODE_H
#include "../common.h"
typedef struct node {
	char name[17]; //null terminated
	uint8_t resv[3];
	uint16_t attrib;
	uint16_t permission;
	uint32_t size;
	uint8_t uid;
	uint8_t child_count;
	uint16_t ID;
	struct node *parent;
	struct node *sib_left;
	struct node *sib_right;
	struct node *first_child; //not used in type 0 or 3, 2 abuses to link
	void *device; //void * so i don't have to deal with circular dependencies
}node_t;

#define VFS_NODE_FILE	    0x00 //first two bits, type attribute,mutaly exclusive
#define VFS_NODE_DIR	    0x01
#define VFS_NODE_LINK       0x02
#define VFS_NODE_OTHER      0x03

#define VFS_NODE_PRIVATE    0x00 //anyone attributes
#define VFS_NODE_READ       0x04
#define VFS_NODE_WRITE      0x08
#define VFS_NODE_EXEC       0x10

#define VFS_NODE_SYSTEM     0x20 //special attributes
#define VFS_NODE_COMPRESSED 0x40
#define VFS_NODE_ENCRYPTED  0x80
#define VFS_NODE_MOUNTPOINT 0x100
#endif

