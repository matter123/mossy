/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef __COMMON_H
#define __COMMON_H
#define LANG_EN_US
//libgcc includes
#include <float.h>
//not including iso646.h
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//typedef for x86
//int8_t  = char
//int16_t = short
//int32_t = int/long
//int64_t = long long

#define no_optimize __attribute__((optimize("O0")))
#define packed_s __attribute__((packed))
#define outdated __attribute__((deprecated))

//some basic asm directives

#define sti() asm("sti")
#define cli() asm("cli")
#define nop() asm("nop")
#define hlt() asm("hlt")

#define end() cli();hlt()

void reboot();

#endif // COMMON_H
