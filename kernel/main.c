/*
 * Copyright 2013 Matthew Fosdick

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "multiboot.h"
#include "common.h"
#include "panic.h"
#include "sleep.h"
#include "mem.h"
#include "keyboard.h"
#include "ps2.h"
#include "debug.h"

#include "boot/multiboot_module.h"
#include "boot/initrd.h"

#include "x86/gdt.h"
#include "x86/idt.h"
#include "x86/irq.h"
#include "x86/pit.h"
#include "x86/VGA.h"
#include "x86/paging.h"

#include "vfs/vfs.h"

#include "x86/key_trans.h"
#include "lang/keycode/usqwerty.h"

#include <stdio.h>
#include <console.h>
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <stream.h>

#include "temp/shell.h"
#include "temp/font.h"

#include <core/graphics.h>

#define PORT 0x3f8   /* COM1 */
void init_serial() {
   outb(PORT + 1, 0x00);    // Disable all interrupts
   outb(PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
   outb(PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
   outb(PORT + 1, 0x00);    //                  (hi byte)
   outb(PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
   outb(PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
   outb(PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set
}

void addTop();
extern void *k_start;
extern void *k_end;

int no_optimize main(multiboot_t *mboot_ptr) {
	//basic setup
	init_gdt();
	init_idt();
	init_modules(mboot_ptr);
	init_serial();
	init_VGA(mboot_ptr);
	initGraphics();
	//gaphical code
	/*things to move:
	 *	multiboot header
	 *  memory map
	 *  vbe info
	 *  vbe mode info
	 *  modules
	 */
	printf("multiboot header=\t%p\nmemory map=\t\t%p\nvbe info=\t\t%p\nvbe mode info=\t\t%p\nkernel start=\t\t%p\nkernel end\t\t%p\n",
		   mboot_ptr,mboot_ptr->mmap_addr,mboot_ptr->vbe_control_info,mboot_ptr->vbe_mode_info,&k_start,&k_end);
	printf("\nmodule count=\t\t%d\n",getModuleCount());
	for(uint32_t i=0;i<getModuleCount();i++) {
		mod_struct_t *cur=getModule(i);
		printf("\tmodule %d start\t%p\n",i,cur->mod_start);
		printf("\t          end  \t%p\n",cur->mod_end);
	}
	printf("\nmboot head len=\t\t%d\n",sizeof(multiboot_t));
	printf("mem map len=\t\t%d\n",mboot_ptr->mmap_length);
	printf("vbe ctrl info len=\t%d\n",sizeof(VBE_info_t));
	printf("vbe mode info len=\t%d\n",sizeof(VBE_mode_t));
	Color clear;
	clear.Red=0xFF;
	clear.Blue=0xFF;
	clear.Green=0xFF;
	ClearScreenSolid(clear);
	clear=(Color){0x00,0x00,0x00,0x00};
	POINT a={20,20};
	POINT b={620,460};
	DrawLine(clear,a,b);
	a=(POINT){620,20};
	b=(POINT){20,460};
	DrawLine(clear,a,b);
	POINT start={640/2-25,480/2-25};
	DrawBoxSolid(ColorFromRGB(0x75,0x4F,0xDF),start,50,50);
	//
	init_mem(mboot_ptr->mmap_addr, mboot_ptr->mmap_length,getEndAddr(),mboot_ptr->mem_upper);
	init_pit(1000);
	//init_paging();
	//clearScreenColor(COLOR_BLACK);
	debug("enabling interrupts\n");
	sti();
	//vfs setup
	init_vfs();
	init_initrd();
	//ps2 setup
	init_ps2();
	ps2_sanity_check();
	
	//keyboard setup
	for (int i=1; i<=2; i++)if (ps2_id(i)==KEYBOARD)init_keyboard(i);
	init_key_trans();
	
	ps2_enable_int();
	enableDebugging();
	
#ifdef LANG_EN_US
	install_USQWERTY();
#endif
	//end();
	init_stdlib();
	//do_graphics();
	//uint8_t* VideoMemory = (uint8_t *)0xA0000;
	//white = 0x3F
	node_t *temp=getRoot();
	temp=findChild(temp,getSystemName("DEVICE.DIR"));
	temp=findChild(temp,getSystemName("INITRD.DIR"));
	node_t *font=findChild(temp,getSystemName("def.fnt"));
	//end();
	end();
	init_font(font);
	init_shell();
	end();
	addTop();
	end();
	//execute shell
	pass_control();
	return 0;
}

void addTop() {
	char spaces[40];
	memset((uint8_t *)spaces,0x20,40);
	//setWriteColor(COLOR_BLACK,COLOR_LGRAY);
	string msg="MOSSY ALPHA VERSION 0.6.5";
	int length=strlen(msg);
	int blength=(40-length)/2;
	spaces[blength]='\0';
	printf("%s%s%s",spaces,msg,spaces);
	if ((40-length)&1)putchar(' ');
	//setWriteColor(COLOR_WHITE,COLOR_BLACK);
}
