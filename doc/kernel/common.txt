includes and defines things that should be present all the time
should be included in every file
should have no dependencies

sti()
    enables interrupts

cli()
    disables interrupts

nop()
    executes a nop (No OPeration)

hlt()
    disables execution until an interrupt

end()
    freezes the machine

reboot()
    reboots the machine, by pulsing the cpu reset line on the keyboard controller

no_optimize
    used in a function to tell gcc to leave the function alone

packed_s
    used in a struct to tell gcc not to pad the struct