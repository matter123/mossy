the keyboard.c file takes an intterupt from the ps/2 port and trsnslates it into a keycode.
the keyboard.c file will also call a keycode interpreter function to translate the keycode
to an ascii char.

init_keyboard(int port)

	initalizes the keyboard to the following default state:

		+scancode set 2
		+all lights disabled
		+typematic rate is 30cps
		+typematic delay is 250ms
		+scanning is enabled

keyboard_install_keycode_interp()

	installs the keycode interpreter function

example keycode translation for us qwerty keyboard (104) keys
all numeric constants are globaly recognized in all languges
a released key is represented by the higest beit being set
ie: 'A' is 0xFFFF8001

0x01: 'A'
0x02: 'B'
0x03: 'C'
0x04: 'D'
0x05: 'E'
0x06: 'F'
0x07: 'G'
0x08: 'H'
0x09: 'I'
0x0A: 'J'
0x0B: 'K'
0x0C: 'L'
0x0D: 'M'
0x0E: 'N'
0x0F: 'O'
0x10: 'P'
0x11: 'Q'
0x12: 'R'
0x13: 'S'
0x14: 'T'
0x15: 'U'
0x16: 'V'
0x17: 'W'
0x18: 'X'
0x19: 'Y'
0x1A: 'Z'

0x1B: '0'
0x1C: '1'
0x1D: '2'
0x1E: '3'
0x1F: '4'
0x20: '5'
0x21: '6'
0x22: '7'
0x23: '8'
0x24: '9'

0x25: 1 //F1
0x26: 2 //F2
0x27: 3 //F3
0x28: 4 //F4
0x29: 5 //F5
0x2A: 6 //F6
0x2B: 7 //F7
0x2C: B //F8 8 was skipped for backspace, 9 for tab, A for \n
0x2D: C //F9
0x2E: E //F10 D skipped carriage return, never typed by the keyboard but still valid ascii char
0x2F: F //F11
0x30: 10 //F12

0x31: 8 //BKSP
0x32: 9 //TAB
0x33: A //NEWLINE
0x34: D //CR

0x35: 11 //UP
0x36: 12 //DOWN
0x37: 13 //LEFT
0x38: 14 //RIGHT

0x39: '`'
0x3A: '-'
0x3B: '='
0x3C: '['
0x3D: ']'
0x3E: '\'
0x3F: ';'
0x40: '''
0x41: ','
0x42: '.'
0x43: '/'

0x44: 15 //CAPS LOCK
0x45: 16 //LSHIFT
0x46: 17 //LMETA
0x47: 18 //LALT
0x48: 19 //RALT
0x49: 1A //RMETA
0x4A: 1C //MENU  1B was skipped for escape
0x4B: 1D //RSHIFT
0x4C: 1E //LCTRL
0x4D: 1F //RCTRL
0x4E: 20 //SPACE

0x4F: 1B //ESC
0x50: 7F //DEL

0x51: 80 //PRNTSCRN
0x52: 81 //SCRLOCK
0x53: 82 //PAUSE
0x54: 83 //NUMLOCK

0x55: 84 //INSERT
0x56: 85 //HOME
0x57: 86 //END
0x58: 87 //PGUP
0x59: 88 //PGDOWN

//NUMBERPAD KEYS
0x5A: 89 //DIV
0x5B: 8A //MUL
0x5C: 8B //SUB
0x5D: 8C //ADD
0x5E: 8D //ENTER
0x5F: 8E //DOT
0x60: 8F //ZERO
0x61: 90 //ONE
0x62: 91 //TWO
0x63: 92 //THREE
0x64: 93 //FOUR
0x65: 94 //FIVE
0x66: 95 //SIX
0x67: 96 //SEVEN
0x68: 97 //EIGHT
0x69: 98 //NINE

//ACPI KEYS
0x6A: 99 //POWER
0x6B: 9A //SLEEP
0x6C: 9B //AWAKE

